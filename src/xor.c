/* rCHL - This is an implementation of random Contrastive Hebbian Learning 
 * algorithm given in: Detorakis, Georgios, Travis Bartley, and Emre Neftci, 
 * "Contrastive Hebbian Learning with Random Feedback Weights",
 *  arXiv preprint arXiv:1806.07406 (2018).
 *  
 *  Copyright (C) 2018  Georgios Is. Detorakis (gdetor@pm.me)
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *  */
#include "common.h"

#define TOL 1e-2


void run_xor(double min, double max, long long int seed, int epochs,
             int samples, int iter, char *fname)
{
    int r, rr, e, idx;
    size_t l, acc, step=500;
    double inputs[4][2] ={{0, 0}, {1, 0}, {0, 1}, {1, 1}};
    double output[4][1] = {{0}, {1}, {1}, {0}};
    double res_o[16], res_y[16]; 
    size_t err_len = (size_t) epochs / step;
    double error[err_len], accur[err_len];
    char *base = NULL;

    layer *lr=NULL;
    layer *tmp_lr=NULL;

    pcg32_random_t rng;
    /* pcg32_srandom_r(&rng, time(NULL), (intptr_t) &rng); */
    pcg32_srandom_r(&rng, seed, seed * 7);

    const size_t L = 3;
    size_t end_ = L - 1;
    double gain[3];
    const double eta = 0.1, gamma = 0.05;
    const double dt = 0.08, tf = 30;
    const size_t sim_time = (size_t) tf / dt;

    lr = alloc(layer, L);
    lr[0].size = 2;
    lr[1].size = 2;
    lr[2].size = 1;

    allocate(lr, min, max, &rng, L);
    lr[1].f = &f;
    lr[2].f = &f;

    if(mlockall(MCL_CURRENT | MCL_FUTURE)) {
        printf("mlockall failed!\n");
    }

    for(l = 1; l < L; ++l) {
        gain[l] = eta * 1.0 / pow(gamma, end_ - l);
        printf("%lf \n", gain[l]);
    }

    printf("Seed is: %lld  \n", seed);
    printf("Start training!\n");
    rr = 0;
    for (r = 0; r < epochs; ++r) {
        for (e = 0; e < 128; ++e) {
            idx = (int) pcg32_boundedrand_r(&rng, 4);
            memcpy(lr[0].x, inputs[idx], sizeof(double) * lr[0].size);
            memcpy(lr[end_].x, output[idx], sizeof(double) * lr[end_].size);
            backward_pass(lr, dt, gamma, L, sim_time);
            forward_pass(lr, dt, gamma, L, sim_time);
            update_weights(lr, gain, L);
        }

        if (r % step == 0) {
            tmp_lr = lr;
            printf("%d  \n", r);
            acc = 0;
            for (e = 0; e < samples; ++e) {
                idx = (int) pcg32_boundedrand_r(&rng, 4);
                res_o[e] = output[idx][0];
                tmp_lr[0].x[0] = inputs[idx][0];
                tmp_lr[0].x[1] = inputs[idx][1];
                for (l = 1; l < L; ++l) {
                    memset(tmp_lr[l].x, 0, sizeof(double) * lr[l].size);
                }
                forward_pass(tmp_lr, dt, gamma, L, sim_time);
                res_y[e] = tmp_lr[2].x[0];
                res_y[e] = tmp_lr[2].x[0] > 0.5 ? 1 : 0;
                if (res_o[e] == res_y[e]) { 
                    acc += 1;
                }
            }
            error[rr] = mse(res_o, res_y, 16);
            accur[rr] = (double) acc / samples;
            printf("Epoch: %d  Error: %lf  Accuracy: %lf\n", r, error[rr], accur[rr]);
            rr++;
        }
    }
    printf("Training done!\n");
    /* write2file("./data/xor/random_error_xor.dat", error, err_len); */
    /* write2file("./data/xor/random_accur_xor.dat", accur, err_len); */

    char err[8]="_error_", ac[8]="_accur_";
    base = get_name(fname, err, iter);
    write2file(base, error, err_len);
    dealloc(base);
    base = get_name(fname, ac, iter);
    write2file(base, accur, err_len);
    dealloc(base);
    /* write2file("./data/xor/random_w1_hebb_xor.dat",
     *            lr[1].W,
     *            lr[1].size * lr[0].size); */
    /* write2file("./data/xor/random_w2_hebb_xor.dat",
     *            lr[2].W,
     *            lr[2].size * lr[1].size); */

    deallocate(lr, L);
    dealloc(lr);
}


int main(int argc, char **argv) 
{   
    if (argc != 3) {
        printf("Invalid number of arguments!\n");
        exit(-1);
    }

    int epochs = 5000;
    int samples = 16;
    double min=-.2, max=.2;
    long long int seed = (long long int) atoi(argv[1]);
    int iter = atoi(argv[2]);
#if RANDOM==1
    char *fname = "./data/xor/xor_rchl";
#else
    char *fname = "./data/xor/xor_chl";
#endif

    printf("---------------------------------\n");
    run_xor(min, max, seed, epochs, samples, iter, fname);
    printf("---------------------------------\n");
    return 0;
}
