/* rCHL - This is an implementation of random Contrastive Hebbian Learning 
 * algorithm given in: Detorakis, Georgios, Travis Bartley, and Emre Neftci, 
 * "Contrastive Hebbian Learning with Random Feedback Weights",
 *  arXiv preprint arXiv:1806.07406 (2018).
 *  
 *  Copyright (C) 2018  Georgios Is. Detorakis (gdetor@pm.me)
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *  */
#include "common.h"

void run_ae(double min_or_mean, double max_or_sigma, long long seed, int epochs,
            int samples_train, int samples_test, int iter, char *fname)
{
    int e, r, step = 1, ii=0, rr=0;
    double **images=NULL;
    double *res_o=NULL, *res_y=NULL;
    size_t l;
    const size_t L = 3;
    const size_t dim[3] = {784, 36, 784};
    double gain[3];
    int size = epochs / step;
    double error[size];
    const double eta = 0.05, gamma = 0.02;
    const double dt = 0.08, tf = 30;
    const size_t sim_time = (size_t) tf / dt;
    const int end_ = L-1;
    char *base=NULL;
    char base_w[50], base_b[50], tmp[15], ext[5]=".dat";

    layer *lr;
    layer *tmp_lr;
    pcg32_random_t rng;
    pcg32_srandom_r(&rng, seed, seed * 7);
    /* pcg32_srandom_r(&rng, time(NULL), (intptr_t) &rng); */

    // double images[5][4] = {{2.0, 1.0, 1.0, 2.0},
    //           {-2.0, 1.0, -1.0, 2.0},
    //           {0.0, 1.0, 0.0, 2.0},
    //           {0.0, -1.0, 0.0, -2.0},
    //           {0.0, -1.0, 0.0, -2.0}};

    // for (int i = 0; i < 5; ++i) {
    //     for (int j = 0; j < 4; ++j) {
    //         images[i][j] /= 2.0;
    //     }
    // }

    char *fname_  = "/share/data/mnist/train-images-idx3-ubyte";
    images = read_mnist_images(samples_train, fname_);

    res_o = alloc(double, samples_test * dim[end_]);
    res_y = alloc(double, samples_test * dim[end_]);

    lr = alloc(layer, L);
    for (l = 0; l < L; ++l)
        lr[l].size = dim[l];
    allocate(lr, min_or_mean, max_or_sigma, &rng, L);
    lr[1].f = &f;
    lr[2].f = &f;

    if(mlockall(MCL_CURRENT | MCL_FUTURE)) {
        printf("mlockall failed!\n");
    }

    for(l = 1; l < L; ++l) {
        gain[l] = eta * 1.0 / pow(gamma, end_ - l);
        printf("%lf  \n", gain[l]);
    }

    printf("Start training!\n");
    for (r = 0; r < epochs; ++r) {
        for (e = 0; e < samples_train; ++e) {
            ii = (int) pcg32_boundedrand(samples_train-1);
            memcpy(lr[0].x, images[ii], sizeof(double) * dim[0]);
            memcpy(lr[end_].x, images[ii], sizeof(double) * dim[end_]);
            backward_pass(lr, dt, gamma, L, sim_time);
            forward_pass(lr, dt, gamma, L, sim_time);
            update_weights(lr, gain, L);
        }

        if (r % step == 0) {
            tmp_lr = lr;
            for (e = 0; e < samples_test; ++e) {
                memcpy(tmp_lr[0].x, images[e], sizeof(double) * dim[0]);
                for (l = 1; l < L; ++l) {
                    memset(tmp_lr[l].x, 0.1, sizeof(double) * lr[l].size);
                }
                forward_pass(tmp_lr, dt, gamma, L, sim_time);
                memcpy(res_o+(e*dim[end_]), images[e], sizeof(double) * dim[end_]);
                memcpy(res_y+(e*dim[end_]), tmp_lr[end_].x, sizeof(double) * dim[end_]);
            }
            error[rr] = mse(res_o, res_y, samples_test*dim[end_]);
            printf("Epoch: %d  Error: %lf\n", r, error[rr]);
            rr++;

            for (l = 1; l < L; ++l) {
#if RANDOM==1
                strcpy(base_w, "./data/ae/best/ae_rchl_w_");
                strcpy(base_b, "./data/ae/best/ae_rchl_b_");
#else
                strcpy(base_w, "./data/ae/best/ae_chl_w_");
                strcpy(base_b, "./data/ae/best/ae_chl_b_");
#endif
                sprintf(tmp, "%d_%d", (int) r, (int) l);
                strcat(base_w, tmp);
                strcat(base_w, ext);
                write2file(base_w, lr[l].W, dim[l]*dim[l-1]);

                strcat(base_b, tmp);
                strcat(base_b, ext);
                write2file(base_b, lr[l].b, dim[l]);
            }
        }
    }
    printf("Training done!\n");

    char err[8]="_error_";
    base = get_name(fname, err, iter);
    write2file(base, error, epochs);
    dealloc(base);

    for (l = 1; l < L; ++l) {
#if RANDOM==1
        strcpy(base_w, "./data/ae/best/ae_rchl_w_");
        strcpy(base_b, "./data/ae/best/ae_rchl_b_");
#else
        strcpy(base_w, "./data/ae/best/ae_chl_w_");
        strcpy(base_b, "./data/ae/best/ae_chl_b_");
#endif
        sprintf(tmp, "%d_%d", (int) iter, (int) l);
        strcat(base_w, tmp);
        strcat(base_w, ext);
        write2file(base_w, lr[l].W, dim[l]*dim[l-1]);

        strcat(base_b, tmp);
        strcat(base_b, ext);
        write2file(base_b, lr[l].b, dim[l]);
    }

    deallocate(lr, L);
    dealloc(lr);
    for(e = 0; e < samples_train; ++e) {
        free(images[e]);
    }
    free(images);
    dealloc(res_o);
    dealloc(res_y);
}


int main(int argc, char **argv)
{
    if (argc != 3) {
        printf("Invalid number of arguments!\n");
        exit(-1);
    }

    /* double min_or_mean=-.3, max_or_sigma=.3; */
    double min_or_mean=-.05, max_or_sigma=.05;
    int num_samples = 10000;
    int num_samples_t = 1000;
    int epochs = 20;
    long long int seed = (long long int) atoi(argv[1]);
    int iter = atoi(argv[2]);
#if RANDOM == 1 
    char *fname = "./data/ae/best/ae_rchl";
#else
    char *fname = "./data/ae/best/ae_chl";
#endif

    printf("---------------------------------\n");
    run_ae(min_or_mean, max_or_sigma, seed, epochs, num_samples, num_samples_t,
           iter, fname);
    printf("---------------------------------\n");
    return 0;
}
