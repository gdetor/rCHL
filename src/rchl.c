/* rCHL - This is an implementation of random Contrastive Hebbian Learning 
 * algorithm given in: Detorakis, Georgios, Travis Bartley, and Emre Neftci, 
 * "Contrastive Hebbian Learning with Random Feedback Weights",
 *  arXiv preprint arXiv:1806.07406 (2018).
 *  
 *  Copyright (C) 2018  Georgios Is. Detorakis (gdetor@pm.me)
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *  */
#include "common.h"

extern inline double mse(double *, double *, size_t);
extern inline double f(double);
extern inline double g(double);
extern inline double h(double);
extern inline double z(double);


/* 
 * Matrix-vector multiplication
 *
 * Argumens
 * --------
 *  A (double *)    Input matrix
 *  x (double *)    Input vector
 *  m (size_t)      Number of rows
 *  n (size_t)      Number of columns
 *
 * Returns
 * -------
 *  y = A x
 *
 */
static inline void dot(double *A, double *x, double **y, size_t m, size_t n)
{
    size_t i, j;
    double mysum = 0;

    for (i = 0; i < m; ++i) {
        mysum = 0;
        for (j = 0; j < n; ++j) {
            mysum += A[i*n+j] * x[j];
        }
        (*y)[i] = mysum;
    }
}


/* 
 * Matrix (transpose)-vector multiplication
 *
 * Argumens
 * --------
 *  A (double *)    Input matrix
 *  x (double *)    Input vector
 *  m (size_t)      Number of rows
 *  n (size_t)      Number of columns
 *
 * Returns
 * -------
 *  y = A^T x
 *
 */
static inline void dot_t(double *A, double *x, double **y, size_t m, size_t n)
{
    size_t i, j;
    double mysum = 0;

    for (i = 0; i < m; ++i) {
        mysum = 0;
        for (j = 0; j < n; ++j) {
            mysum += A[j*m+i] * x[j];        
        }
        (*y)[i] = mysum;
    }
}


/* 
 * CHL and rCHL Free phase
 *
 * Argumens
 * --------
 *  lr (layer)          Network layer data struct
 *  dt (double)         Forward Euler time step
 *  gamma (double)      Feedback gain     
 *  L (size_t)          Number layers
 *  sim_time (size_t)   Simulation time
 *
 * Returns
 * -------
 *  Void
 *
 */
void forward_pass(layer *restrict lr,
                  const double dt,
                  const double gamma,
                  const size_t L,
                  const size_t sim_time)
{
    size_t i, t, l;

    dot(lr[1].W, lr[0].x, &lr[1].tmp, lr[1].size, lr[0].size);

    for (t = 0; t < sim_time; ++t) {
        for (l = 1; l < L-1; ++l) {
            if (l > 1) {
                dot(lr[l].W, lr[l-1].x, &lr[l].tmp, lr[l].size, lr[l-1].size);
            }

#if RANDOM == 1
            dot_t(lr[l+1].G, lr[l+1].x, &lr[l].tmp_t, lr[l].size, lr[l+1].size);
#else
            dot_t(lr[l+1].W, lr[l+1].x, &lr[l].tmp_t, lr[l].size, lr[l+1].size);
#endif
            for (i = 0; i < lr[l].size; ++i) {
                lr[l].x[i] += dt * (-lr[l].x[i] + lr[l].f(lr[l].tmp[i] + gamma * lr[l].tmp_t[i] + lr[l].b[i]));
            }
        }

        dot(lr[L-1].W, lr[L-2].x, &lr[L-1].tmp, lr[L-1].size, lr[L-2].size);

        for (i = 0; i < lr[L-1].size; ++i) {
            lr[L-1].x[i] += dt * (-lr[L-1].x[i] + lr[L-1].f(lr[L-1].tmp[i] + lr[L-1].b[i]));
        }
    }

    for (l = 0; l < L; ++l) {
        memcpy(lr[l].x_neg, lr[l].x, sizeof(double) * lr[l].size);
    }

#if BIAS == 1
    for (l = 1; l < L; ++l) {
        memcpy(lr[l].db_chk, lr[l].x, sizeof(double) * lr[l].size);
    }
#endif
} 


/* 
 * CHL and rCHL Clamped phase
 *
 * Argumens
 * --------
 *  lr (layer)          Network layer data struct
 *  dt (double)         Forward Euler time step
 *  gamma (double)      Feedback gain     
 *  L (size_t)          Number layers
 *  sim_time (size_t)   Simulation time
 *
 * Returns
 * -------
 *  Void
 *
 */
void backward_pass(layer *restrict lr,
                   const double dt,
                   const double gamma,
                   const size_t L,
                   const size_t sim_time)
{
    size_t i, t, l;

    dot(lr[1].W, lr[0].x, &lr[1].tmp, lr[1].size, lr[0].size);
#if RANDOM == 1
    dot_t(lr[L-1].G, lr[L-1].x, &lr[L-2].tmp_t, lr[L-2].size, lr[L-1].size);
#else
    dot_t(lr[L-1].W, lr[L-1].x, &lr[L-2].tmp_t, lr[L-2].size, lr[L-1].size);
#endif

    for (t = 0; t < sim_time; ++t) {
        for (l = 1; l < L-1; ++l) {
            if (l > 1) {
                dot(lr[l].W, lr[l-1].x, &lr[l].tmp, lr[l].size, lr[l-1].size);
            }

            if (l < L-1) {
#if RANDOM == 1
                dot_t(lr[l+1].G, lr[l+1].x, &lr[l].tmp_t, lr[l].size, lr[l+1].size);
#else
                dot_t(lr[l+1].W, lr[l+1].x, &lr[l].tmp_t, lr[l].size, lr[l+1].size);
#endif
            }

            for (i = 0; i < lr[l].size; ++i) {
                lr[l].x[i] += dt * (-lr[l].x[i] + lr[l].f(lr[l].tmp[i] + gamma * lr[l].tmp_t[i] + lr[l].b[i]));
            }
        }
    }

    for (l = 0; l < L; ++l) {
        memcpy(lr[l].x_pos, lr[l].x, sizeof(double) * lr[l].size);
    }

#if BIAS == 1
    for (l = 1; l < L; ++l) {
        memcpy(lr[l].db_hat, lr[l].x, sizeof(double) * lr[l].size);
    }
#endif
}


/* 
 * CHL and rCHL weights update
 *
 * Argumens
 * --------
 *  lr (layer)          Network layer data struct
 *  gain (double)       lrate × gamma^[k-L], k is the current layer, L is the
 *                      total number of layers
 *  L (size_t)          Number layers
 *
 * Returns
 * -------
 *  Void
 *
 */
void update_weights(layer *restrict lr,
                    double *gain,
                    const size_t L)
{
    size_t i, j, l;
    double pos, neg;
    
    for (l = 1; l < L; ++l) {
        for(i = 0; i < lr[l].size; ++i) {
            for (j = 0; j < lr[l-1].size; ++j) {
                pos = lr[l].x_pos[i] * lr[l-1].x_pos[j];
                neg = lr[l].x_neg[i] * lr[l-1].x_neg[j];
                lr[l].W[i*lr[l-1].size+j] += gain[l] * (pos - neg);
            }
#if BIAS == 1
            lr[l].b[i] += gain[l] * (lr[l].db_hat[i] - lr[l].db_chk[i]);
#endif
        }
    }
}


/* Memory allocation routine */
void allocate(layer *restrict lr, double min, double max, pcg32_random_t *rng,
              const size_t L)
{
    size_t l, len;
#if RANDOM == 1
    double min_, max_;
#if NORMAL == 1
    double sigma=0, mean=0;
#endif
#endif

    for (l = 0; l < L; ++l) {
        lr[l].x = alloc_zeros(double, lr[l].size);
        lr[l].x_pos = alloc_zeros(double, lr[l].size);
        lr[l].x_neg = alloc_zeros(double, lr[l].size);
        if (l > 0 ) {
            len = lr[l-1].size * lr[l].size;            
            lr[l].b = alloc_zeros(double, lr[l].size);
#if BIAS == 0
            memset(lr[l].b, 0, sizeof(double) * lr[l].size);
#else
            uniform_array(&lr[l].b, -0.9, 0.9, lr[l].size, rng);
#endif
            lr[l].tmp = alloc_zeros(double, lr[l].size);
            lr[l].tmp_t = alloc_zeros(double, lr[l].size);
            lr[l].db_hat = alloc_zeros(double, lr[l].size);
            lr[l].db_chk = alloc_zeros(double, lr[l].size);
            lr[l].dW_hat = alloc_zeros(double, len);
            lr[l].dW_chk = alloc_zeros(double, len);
            lr[l].W = alloc_zeros(double, len);
            uniform_array(&lr[l].W, -.5, .5, len, rng);
#if RANDOM == 1
            lr[l].G = alloc_zeros(double, len);
#if NORMAL == 1
            /* sigma = 1.0 / sqrt(len); */
            sigma = max;
            mean = min;
            normal_array(&lr[l].G, mean, sigma, len);
            printf("Initialized matrices G using a Normal!\n");
#else
            uniform_array(&lr[l].G, min, max, len, rng);
            printf("Initialized matrices G using a Uniform!\n");
#endif
#endif
        } else {
            lr[l].b = 0;
            lr[l].db_hat = NULL; 
            lr[l].db_chk = NULL; 
            lr[l].dW_hat = NULL;
            lr[l].dW_chk = NULL;
            lr[l].W = NULL;
#if RANDOM == 1
            lr[l].G = NULL;
#endif
        }
    }

#if RANDOM == 1
    for (l = 1; l < L-1; ++l) {
        printf("-------------------------\n");
        svd_(lr[l].G, lr[l].size, lr[l+1].size, &min_, &max_);
        // spectral_radius(&lr[l].G, lr[l].size, lr[l+1].size);
        printf("-------------------------\n");
    }
#endif
}


/* Memory deallocation routine */
void deallocate(layer *restrict lr, const size_t L)
{
    size_t l;
    for (l = 0; l < L; ++l) {
        dealloc(lr[l].x);
        dealloc(lr[l].x_pos);
        dealloc(lr[l].x_neg);
        if (l > 0) {
            dealloc(lr[l].b);
            dealloc(lr[l].tmp);
            dealloc(lr[l].tmp_t);
            dealloc(lr[l].db_hat);
            dealloc(lr[l].db_chk);
            dealloc(lr[l].dW_hat);
            dealloc(lr[l].dW_chk);
            dealloc(lr[l].W);
#if RANDOM == 1
            dealloc(lr[l].G);
#endif
        }
    }
}


/* SVD - Requires BLAS */
void svd_(double *x, const size_t size_p, const size_t size_n, double *min_,
          double *max_)
{
    size_t len1 = size_n * size_p;
    double *s = alloc_zeros(double, size_n);
    double *tmp = alloc_zeros(double, len1);
    double *u = alloc_zeros(double, size_n * size_n);
    double *vt = alloc_zeros(double, size_p * size_p);
    double *work = NULL, wkopt;
    int info, lwork=-1;

    memcpy(tmp, x, sizeof(double) * len1);
    dgesvd_("N", "N", (int *) &size_p, (int *)&size_n, tmp, (int *)&size_p,
            s, u, (int *) &size_n, vt, (int *)&size_n,  &wkopt, &lwork, &info);
    lwork = (int)wkopt;
    work = alloc(double, lwork);
    dgesvd_("N", "N", (int *) &size_p, (int *)&size_n, tmp, (int *) &size_p,
            s, u, (int *) &size_p, vt, (int *) &size_n,  work, &lwork, &info);
    /* for(i = 0; i < size_n; ++i) */
    /*     printf("SVD:   %lf  \n", s[i]); */
    
    *max_ = max(s, size_n);
    *min_ = min(s, size_n);
    printf("Largest s: %lf, Smallest s: %lf \n", *max_, *min_);

    dealloc(tmp);
    dealloc(work);
    dealloc(u);
    dealloc(vt);
    dealloc(s);
}


/* Spectral radius  -Requires BLAS */
void spectral_radius(double **x, const size_t size_p, const size_t size_n)
{
    size_t i, j;
    double min_, max_;
    svd_(*x, size_p, size_n, &min_, &max_);
    
    for (i = 0; i < size_p; ++i) {
        for (j = 0; j < size_n; ++j) {
            (*x)[i*size_n+j] /= max_;
        }
    }
}
