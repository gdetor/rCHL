/* rCHL - This is an implementation of random Contrastive Hebbian Learning 
 * algorithm given in: Detorakis, Georgios, Travis Bartley, and Emre Neftci, 
 * "Contrastive Hebbian Learning with Random Feedback Weights",
 *  arXiv preprint arXiv:1806.07406 (2018).
 *  
 *  Copyright (C) 2018  Georgios Is. Detorakis (gdetor@pm.me)
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *  */
#include "common.h"

extern inline int reverse_int (int);


double **read_mnist_images(size_t num_images, char *fname) {
    int n_rows=0, n_cols=0;
    int magic_number = 0;
    int number_of_images = 0;

    unsigned char temp = 0;
    double **tmp = NULL;

    FILE *fp = NULL;
    if (!(fp = fopen(fname, "rb"))) {
        printf("File not found!\n");
        exit(-1);
    }

    fread((char*)&magic_number, sizeof(magic_number), 1, fp);
    magic_number = reverse_int(magic_number);

    fread((char*)&number_of_images, sizeof(number_of_images), 1, fp);
    number_of_images = reverse_int(number_of_images);
    printf("Number of total Images: %d  \n", number_of_images);

    fread((char*)&n_rows, sizeof(n_rows), 1, fp);
    n_rows = reverse_int(n_rows);

    fread((char*)&n_cols, sizeof(n_cols), 1, fp);
    n_cols = reverse_int(n_cols);

    tmp = (double **)malloc(num_images * sizeof(double *));
    for(int i = 0; i < num_images; ++i) {
        tmp[i] = (double *)malloc(n_rows * n_cols * sizeof(double));
    }

    for(int i = 0; i < num_images; ++i) {
        for(int r = 0; r < n_rows; ++r) {
            for(int c = 0; c < n_cols; ++c) {
                temp = 0;
                fread((char*)&temp, 1, sizeof(temp), fp);
                tmp[i][(n_cols*r)+c] = (double)temp / 255.0;
            }
        }
    }

    fclose(fp);
    fp = NULL;

    return tmp;
}


double **read_mnist_labels(size_t num_images, size_t len, char *fname) {
    int n_labels = 0;
    int magic_number = 0;
    int index;

    unsigned char temp = 0;
    double **tmp = NULL;

    FILE *fp = NULL;
    if (!(fp = fopen(fname, "rb"))) {
        printf("File not found!\n");
        exit(-1);
    }

    fread((char*)&magic_number, sizeof(magic_number), 1, fp);
    magic_number = reverse_int(magic_number);

    fread((char*)&n_labels, sizeof(n_labels), 1, fp);
    n_labels = reverse_int(n_labels);
    printf("Number of total Labels: %d  \n", n_labels);

    tmp = (double **)malloc(num_images * sizeof(double *));
    for(int i = 0; i < num_images; ++i) {
        tmp[i] = (double *)malloc(len * sizeof(double));
    }

    for(int i = 0; i < num_images; ++i) {
        temp = 0;
        fread((char*)&temp, 1, sizeof(temp), fp);
        index = (int)temp;
        for(int j = 0; j < len; ++j) {
            tmp[i][j] = 0;
            if (j == index) {
                tmp[i][j] = 1;
            }
        }
    }

    fclose(fp);
    fp = NULL;

    return tmp;
}


void read_bst_images(char *fname, double ***x, double ***label, size_t n,
                     size_t m)
{
    size_t i, j;	
    double *tmp = NULL;
    FILE *fp = NULL;

    if(!(fp = fopen(fname, "rb"))) {
        printf("File %s not found!\n", fname);
        exit(-1);
    }

    tmp = alloc_zeros(double, n * m);
    fread(tmp, sizeof(double), m * n, fp);

    for (i = 0; i < n; ++i) {
        for (j = 0; j < m; ++j) {
            (*x)[i][j] = tmp[i*m + j];
        }
        if (i < 16) {
            (*label)[i][0] = 1;
            (*label)[i][1] = 0;
        } else {
            (*label)[i][0] = 0;
            (*label)[i][1] = 1;
        }
    }

    fclose(fp);
    dealloc(tmp);
}


double **read_sin_data(char *fname, int *win_size, int *num_samples)
{
    int i, j;
    int ii, jj, tt;
    double **x=NULL, *tmp=NULL;
    FILE *fp=NULL;

    if(!(fp = fopen(fname, "rb"))) {
        printf("File %s not found!\n", fname);
        exit(-1);
    }

    fread(&ii, sizeof(int), 1, fp);     /* Samples  */
    fread(&jj, sizeof(int), 1, fp);     /* win size */
    fread(&tt, sizeof(int), 1, fp);     /* win size */
    x = (double **)malloc(ii * sizeof(double *));
    for (i = 0; i < ii; ++i) {
        x[i] = (double *)malloc(jj * sizeof(double));
    }

    tmp = (double *)malloc(ii * jj * sizeof(double));
    fread(tmp, sizeof(double), jj * ii, fp);
    fclose(fp);

    for(i = 0; i < ii; ++i) {
        for(j = 0; j < jj; ++j) {
            x[i][j] = tmp[i * jj + j];
        }
    }

    *num_samples = ii;
    *win_size = jj;

    free(tmp);
    return x;
}
