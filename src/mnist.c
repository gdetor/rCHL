/* rCHL - This is an implementation of random Contrastive Hebbian Learning 
 * algorithm given in: Detorakis, Georgios, Travis Bartley, and Emre Neftci, 
 * "Contrastive Hebbian Learning with Random Feedback Weights",
 *  arXiv preprint arXiv:1806.07406 (2018).
 *  
 *  Copyright (C) 2018  Georgios Is. Detorakis (gdetor@pm.me)
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *  */
#include "common.h"

#define NET_SIZE 4

void run_mnist(double min_or_mean,
               double max_or_sigma,
               long long int seed,
               int epochs,
               int samples_train,
               int samples_test,
               int iter,
               char *fname)
{
    int e, r, rr;
    double **images, **images_test = NULL;
    double **labels, **labels_test = NULL;
    double *res_o=NULL, *res_y=NULL;
    int step = 1, ii;
    int size = epochs / step;
    double error[size], accur[size];
    int m_o, m_y, acc;
    size_t l;
    int *idx=NULL;
    size_t L = NET_SIZE;
    const size_t dim[NET_SIZE] = {784, 128, 64, 10};
    double gain[NET_SIZE];
    const double eta = 0.01, gamma = 0.05;
    const double dt = 0.08, tf = 25;
    const size_t sim_time = (size_t) tf / dt;
    const int end_ = L-1;
    char *base=NULL;

    layer *lr;
    layer *tmp_lr;

    pcg32_random_t rng;
    pcg32_srandom_r(&rng, seed, seed / 7);
    /* pcg32_srandom_r(&rng, time(NULL), (intptr_t) &rng); */

    char *fname_  = "/share/data/mnist/train-images-idx3-ubyte";
    images = read_mnist_images(samples_train, fname_);
    fname_  = "/share/data/mnist/t10k-images-idx3-ubyte";
    images_test = read_mnist_images(samples_test, fname_);
    fname_  = "/share/data/mnist/train-labels-idx1-ubyte";
    labels = read_mnist_labels(samples_train, dim[end_], fname_);
    fname_  = "/share/data/mnist/t10k-labels-idx1-ubyte";
    labels_test = read_mnist_labels(samples_test, dim[end_], fname_);

    res_o = alloc(double, samples_test * dim[end_]);
    res_y = alloc(double, samples_test * dim[end_]);

    idx = alloc(int, samples_train * epochs);

    lr = alloc(layer, L);
    for (l = 0; l < L; ++l)
        lr[l].size = dim[l];
    allocate(lr, min_or_mean, max_or_sigma, &rng, L);
    lr[1].f = &f;
    lr[2].f = &f;
    lr[3].f = &f;

    if(mlockall(MCL_CURRENT | MCL_FUTURE)) {
        printf("mlockall failed!\n");
    }

    for (l = 1; l < L; ++l)
        gain[l] = eta * 1.0 / pow(gamma, end_ - l);

    for (l = 0; l < epochs*samples_train; ++l) {
       idx[l] = (int) pcg32_boundedrand(samples_train-1);
    }

    printf("Start training!\n");
    rr = 0;
    for (r = 0; r < epochs; ++r) {
        for (e = 0; e < samples_train; ++e) {
            ii = idx[r*samples_train+e];
            memcpy(lr[0].x, images[ii], sizeof(double) * dim[0]);
            memcpy(lr[end_].x, labels[ii], sizeof(double) * dim[end_]);
            backward_pass(lr, dt, gamma, L, sim_time);
            forward_pass(lr, dt, gamma, L, sim_time);
            update_weights(lr, gain, L);
        }

        if (r % step == 0) {
            tmp_lr = lr;
            acc = 0;
            for (e = 0; e < samples_test; ++e) {
                memcpy(tmp_lr[0].x, images_test[e], sizeof(double) * dim[0]);
                for (l = 1; l < L; ++l) {
                    memset(tmp_lr[l].x, 0.1, sizeof(double) * lr[l].size);
                }
                forward_pass(tmp_lr, dt, gamma, L, sim_time);
                m_o = argmax(labels_test[e], dim[end_]);
                m_y = argmax(tmp_lr[end_].x, dim[end_]);
                if (m_o == m_y)
                     acc += 1;
                memcpy(res_o+(e*dim[end_]), labels_test[e], sizeof(double) * dim[end_]);
                memcpy(res_y+(e*dim[end_]), tmp_lr[end_].x, sizeof(double) * dim[end_]);
            }
            error[rr] = mse(res_o, res_y, samples_test*dim[end_]);
            accur[rr] = (double) acc / samples_test;
            printf("Epoch: %d  Error: %lf  Accuracy: %lf\n", r, error[rr], accur[rr]);
            rr++;
        }
    }
    printf("Training done!\n");

    char err[8]="_error_", ac[8]="_accur_";
    base = get_name(fname, err, iter);
    write2file(base, error, epochs);
    dealloc(base);
    base = get_name(fname, ac, iter);
    write2file(base, accur, epochs);
    dealloc(base);
    /* write2file("./data/mnist/std_error_mnist.dat", error, epochs); */
    /* write2file("./data/mnist/std_accur_mnist.dat", accur, epochs); */
    /* write2file("./data/mnist/random_nobias_error_mnist.dat", error, epochs); */
    /* write2file("./data/mnist/random_nobias_accur_mnist.dat", accur, epochs); */
    /* write2file("./data/w1_hebb_mnist.dat", lr[1].W, dim[1]*dim[0]); */
    /* write2file("./data/w2_hebb_mnist.dat", lr[2].W, dim[2]*dim[1]); */

    deallocate(lr, L);
    dealloc(lr);
    for(e = 0; e < samples_train; ++e) {
        free(images[e]);
        free(labels[e]);
    }
    free(images);
    free(labels);
    for(e = 0; e < samples_test; ++e) {
        free(images_test[e]);
        free(labels_test[e]);
    }
    free(images_test);
    free(labels_test);
    dealloc(res_o);
    dealloc(res_y);
}


int main(int argc, char **argv)
{
    if (argc != 3) {
        printf("Invalid number of arguments!\n");
        exit(-1);
    }

    double min_or_mean=-.6, max_or_sigma=.6;
    int num_samples = 60000;
    int num_samples_t = 10000;
    int epochs = 40;
    long long int seed = (long long int) atoi(argv[1]);
    int iter = atoi(argv[2]);
#if RANDOM == 1
    char *fname = "./data/mnist/mnist_rchl";
#else
    char *fname = "./data/mnist/mnist_chl";
#endif

    printf("---------------------------------\n");
    run_mnist(min_or_mean, max_or_sigma, seed, epochs, num_samples,
              num_samples_t, iter, fname);
    printf("---------------------------------\n");
    return 0;
}
