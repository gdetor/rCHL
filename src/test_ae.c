/* rCHL - This is an implementation of random Contrastive Hebbian Learning 
 * algorithm given in: Detorakis, Georgios, Travis Bartley, and Emre Neftci, 
 * "Contrastive Hebbian Learning with Random Feedback Weights",
 *  arXiv preprint arXiv:1806.07406 (2018).
 *  
 *  Copyright (C) 2018  Georgios Is. Detorakis (gdetor@pm.me)
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *  */
#include "common.h"


void test_autoencoder(double min_or_mean, double max_or_sigma) 
{
    int L = 3;
    size_t i, samples=5000;
    double **images=NULL, **labels=NULL;
    double dt = 0.08, tf = 30, gamma = 0.02;
    size_t sim_time = (size_t) tf / dt;
    size_t num_digits = 1000;
    char base_e[50], base_a[50], tmp[15];
    char ext[5] = ".dat";
    double targets[1000];
    
    layer *lr;
    pcg32_random_t rng;

    lr = alloc(layer, L);
    lr[0].size = 784;
    lr[1].size = 36;
    lr[2].size = 784;
    allocate(lr, min_or_mean, max_or_sigma, &rng, L);

    if(mlockall(MCL_CURRENT | MCL_FUTURE)) {
        printf("mlockall failed!\n");
    }

    lr[1].f = &f;
    lr[2].f = &f;

    char *fname  = "/share/data/mnist/train-images-idx3-ubyte";
    images = read_mnist_images(samples, fname);
    fname  = "/share/data/mnist/train-labels-idx1-ubyte";
    labels = read_mnist_labels(samples, 10, fname);

#if RANDOM==1
    read_from_file("./data/ae/ae_rchl_w_5_1.dat", 
                    &lr[1].W, lr[1].size*lr[0].size);
    read_from_file("./data/ae/ae_rchl_w_5_2.dat",
                    &lr[2].W, lr[2].size*lr[1].size);
    read_from_file("./data/ae/ae_rchl_b_5_1.dat",
                    &lr[1].b, lr[1].size);
    read_from_file("./data/ae/ae_rchl_b_5_2.dat",
                    &lr[2].b, lr[2].size);
#else
    read_from_file("./data/ae/ae_chl_w_9_1.dat", 
                    &lr[1].W, lr[1].size*lr[0].size);
    read_from_file("./data/ae/ae_chl_w_9_2.dat",
                    &lr[2].W, lr[2].size*lr[1].size);
    read_from_file("./data/ae/ae_chl_b_9_1.dat",
                    &lr[1].b, lr[1].size);
    read_from_file("./data/ae/ae_chl_b_9_2.dat",
                    &lr[2].b, lr[2].size);
#endif

    for (i = 0; i < num_digits; ++i) {
        /* mask_data(&images[i], 28, 28, 157); */
        memcpy(lr[0].x, images[i], sizeof(double) * lr[0].size);
        memset(lr[2].x, 0.0, lr[2].size);
        forward_pass(lr, dt, gamma, L, sim_time);
        targets[i] = argmax(labels[i], 10);
#if RANDOM==1
        strcpy(base_e, "./data/ae/ae_rchl_test_image_");
        strcpy(base_a, "./data/ae/ae_rchl_test_recon_");
#else
        strcpy(base_e, "./data/ae/ae_chl_test_image_");
        strcpy(base_a, "./data/ae/ae_chl_test_recon_");
#endif
        sprintf(tmp, "%ld", i);
        strcat(base_e, tmp);
        strcat(base_e, ext);
        write2file(base_e, images[i], lr[L-1].size);
        strcat(base_a, tmp);
        strcat(base_a, ext);
        write2file(base_a, lr[L-1].x, lr[L-1].size);
    }

    write2file("./data/ae/targets.dat", targets, samples);

    for (i = 0; i < samples; ++i)
        free(images[i]);
    free(images);
    for (i = 0; i < samples; ++i)
        free(labels[i]);
    free(labels);
    deallocate(lr, L);
}


int main()
{
    double min_or_mean=-.1, max_or_sigma=.1;
    test_autoencoder(min_or_mean, max_or_sigma);
    return 0;
}
