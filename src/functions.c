/* rCHL - This is an implementation of random Contrastive Hebbian Learning 
 * algorithm given in: Detorakis, Georgios, Travis Bartley, and Emre Neftci, 
 * "Contrastive Hebbian Learning with Random Feedback Weights",
 * arXiv preprint arXiv:1806.07406 (2018).
 *  
 * Copyright (C) 2018  Georgios Is. Detorakis (gdetor@pm.me)
 *  
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * */
#include "common.h"


extern inline int argmax(double *, size_t);
extern inline int argmin(double *, size_t);
extern inline double max(double *, size_t);
extern inline double min(double *, size_t);


char *get_name(char *path, char *type, long long int index)
{
    char *base, tmp[30];
    char ext[5] = ".dat";

    base = alloc(char, 150);

    strcpy(base, path);
    strcat(base, type);
    sprintf(tmp, "%lld", index);
    strcat(base, tmp);
    strcat(base, ext);

    return base;
}


char *concatenate(char *path, double max)
{
    char *base, tmp[20];
    char ext[5] = ".dat";

    base = alloc(char, 80);

    strcpy(base, path);
    sprintf(tmp, "%lf", max);
    strcat(base, tmp);
    strcat(base, ext);

    return base;
}


void write2file(char *fname, double *restrict x, size_t len)
{
    FILE *fp = NULL;

    if(!(fp = fopen(fname, "wb"))) {
        printf("Cannot open file %s!\n", fname);
        exit(-1);
    }

    fwrite(x, sizeof(double), len, fp);
    fclose(fp);
    fp = NULL;
}


void read_from_file(char *fname, double **restrict x, size_t len)
{
    FILE *fp = NULL;

    if(!(fp = fopen(fname, "rb"))) {
        printf("Cannot find hile %s !\n", fname);
        exit(-1);
    }

    fread(*x, sizeof(double), len, fp);

    fclose(fp);
    fp = NULL;
}


void transpose(char *fname, double **restrict x, size_t m, size_t n)
{
    size_t i, j;
    FILE *fp;
    double tmp[m*n];

    if(!(fp = fopen(fname, "rb"))) {
        printf("Cannot find hile %s !\n", fname);
        exit(-1);
    }

    fread(&tmp, sizeof(double), m*n, fp);
    fclose(fp);

    for (j = 0; j < n; ++j) {
        for (i = 0; i < m; ++i) {
            (*x)[j*m+i] = tmp[i*n+j];
        }
    }
}


void mask_data(double **restrict data, size_t m, size_t n, int perc)
{
    size_t i, j, idx, count = 0;
    pcg32_random_t rng;

    for (i = 0; i < m; ++i) {
        for(j = 0; j < n; ++j) {
            count++;
            if (count > perc) { break; }
            idx = (size_t) pcg32_boundedrand_r(&rng, m*n);
            (*data)[idx] = 0.0;
        }
    }
}


/* ***********************************************************************
 * UNIFORM  Returns a uniform number in the interval [low, upper). It
 * uses the PCG random number generator.
 *
 * Args:
 *  low (double)             : Lower boundary
 *  upper (double)           : Upper boundary
 *  rng (*pcg32_random_t)    : Pointer to the Random number generator 
 *
 * Return:
 *  Double -- A random number uniformly drawn from [low, upper).
 * ***********************************************************************/
double uniform(double low, double upper, pcg32_random_t *rng)
{
    double rand_n = pcg32_random_r(rng) / (1.0 + UINT32_MAX);
    double range = (upper - low);
    return (rand_n * range) + low;
}


/* ***********************************************************************
 * UNIFORM_ARRAY  Fills in an array with random numbers (uniformly 
 * distributed).
 *
 * Args:
 *  x (double **)           : Double array to fill in
 *  a (double a)            : Lower boundary
 *  b (double b)            : Upper boundary
 *  len (size_t)            : The size of the array
 *  rng (pcg32_random_t)    : Pointer to the RNG
 *
 * Return:
 *
 * ***********************************************************************/
void uniform_array(double **restrict x, double a, double b, size_t len,
                   pcg32_random_t *rng)
{
    size_t i;

    for(i = 0; i < len; ++i) {
        (*x)[i] = uniform(a, b, rng);
    }
}


/* ***********************************************************************
 * NORMAL Returns a double drawn from a normal distribution based on 
 * Box-Muller. 
 *
 * Args:
 *  mu (double)     : The mean of the distribution
 *  sigma (double)  : The variance of the distribution
 *
 * Return:
 *  Double -- Drawn from a normal distribution.
 * ***********************************************************************/
double normal(double mu, double sigma)
{
	const double epsilon = DBL_MIN;
	const double two_pi = 2.0*3.14159265358979323846;

	static double z0, z1;
	static bool generate;
	generate = !generate;

	if (!generate)
	   return z1 * sigma + mu;

	double u1, u2;
	do
	 {
       u1 = (double) pcg32_boundedrand(100) / (double) 100;
       u2 = (double) pcg32_boundedrand(100) / (double) 100;
	 }
	while ( u1 <= epsilon );

	z0 = sqrt(-2.0 * log(u1)) * cos(two_pi * u2);
	z1 = sqrt(-2.0 * log(u1)) * sin(two_pi * u2);
	return z0 * sigma + mu;
}


/* ***********************************************************************
 * NORMAL_ARRAY  Fills in an array with random numbers drawn from a 
 * normal distribution.
 *
 * Args:
 *  x (double **)           : Array to populate
 *  mean (double)           : Mean of the distribution
 *  sigma (double)          : Variance of the distribution
 *  len  (size_t)           : Size of the array
 *  rng (pcg32_random_t *)  : Pointer to RNG
 * Return:
 * ***********************************************************************/
void normal_array(double **x, double mean, double sigma, size_t len)
{
    size_t i;

    for(i = 0; i < len; ++i) {
        (*x)[i] = normal(mean, sigma);
    }
}


void print_mat(double *x, size_t rows, size_t cols)
{
    size_t i, j;

    for(i = 0; i < rows; ++i) {
        for(j = 0; j < cols; ++j) {
            printf("%lf  ", x[i*cols+j]);
        }
        printf("\n");
    }

}
