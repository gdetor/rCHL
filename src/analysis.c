/* rCHL - This is an implementation of random Contrastive Hebbian Learning 
 * algorithm given in: Detorakis, Georgios, Travis Bartley, and Emre Neftci, 
 * "Contrastive Hebbian Learning with Random Feedback Weights",
 *  arXiv preprint arXiv:1806.07406 (2018).
 *  
 *  Copyright (C) 2018  Georgios Is. Detorakis (gdetor@pm.me)
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *  */
#include "common.h"

void run_bars_stipes(double gamma,
                     double eta,
                     double min_or_mean,
                     double max_or_sigma,
                     long long int seed,
                     int epochs,
                     int iter,
                     char *fname)
{
    int e, r, rr, k;
    int idx;
    double **images=NULL;
    double **labels=NULL;
    double *res_o=NULL, *res_y=NULL;
    int step = 1;
    int size = epochs / step;
    double error[size], accur[size];
    int m_o, m_y, acc;
    size_t l;
    const size_t L = 3;
    const size_t dim[3] = {16, 50, 2};
    double gain[3];
    const double dt = 0.08, tf = 30;
    const size_t sim_time = (size_t) tf / dt;
    const int end_ = L-1;
    char *base=NULL;

    layer *lr=NULL;
    layer *tmp_lr=NULL;

    pcg32_random_t rng;
    // pcg32_srandom_r(&rng, time(NULL), (intptr_t) &rng);
    pcg32_srandom_r(&rng, seed, 7);

    res_o = alloc(double, 32 * dim[end_]);
    res_y = alloc(double, 32 * dim[end_]);

    lr = alloc(layer, L);
    for (l = 0; l < L; ++l) lr[l].size = dim[l];
    allocate(lr, min_or_mean, max_or_sigma, &rng, L);

#if RANDOM==1
    write2file("./data/analysis/analysis_rchl_fdbk_matrix.dat",
               lr[2].G, dim[1] * dim[2]);
#endif
    write2file("./data/analysis/analysis_rchl_initial_w1.dat",
               lr[1].W, dim[1] * dim[0]);
    write2file("./data/analysis/analysis_rchl_initial_w2.dat",
               lr[2].W, dim[1] * dim[2]);

    // base = "./data/bars_stripes/w1_hebb_bars_stripes_init.dat";
    /* base = "./data/bars_stripes/w1_hebb_bars_stripes_init.dat"; */
    /* read_from_file(base, &lr[1].W, lr[0].size * lr[1].size); */
    // write2file(base, lr[1].W, lr[0].size*lr[1].size);
    /* dealloc(base); */
    // base = "./data/bars_stripes/w2_hebb_bars_stripes_init.dat";
    /* base = "./data/bars_stripes/w2_hebb_bars_stripes_init.dat"; */
    /* read_from_file(base, &lr[1].W, lr[1].size * lr[2].size); */
    // write2file(base, lr[2].W, lr[1].size*lr[2].size);
    /* dealloc(base); */
    // base = "./data/bars_stripes/w2_hebb_bars_stripes_init.dat";
    /* base = "./data/bars_stripes/w2_hebb_bars_stripes_init.dat"; */
    /* read_from_file(base, &lr[1].G, lr[1].size * lr[2].size); */

    /* lr[1].f = &f; */
    /* lr[2].f = &f; */
    /* lr[3].f = &f; */
    /* lr[4].f = &f; */
    /* lr[5].f = &f; */

    for(l = 1; l < L; ++l) {
        lr[l].f = &f;
        gain[l] = eta * 1.0 / pow(gamma, end_ - l);
        printf("%lf  \n", gain[l]);
    }
    
    /* Load input samples */
    labels = (double **)malloc(sizeof(double *) * 32);
    images = (double **)malloc(sizeof(double *) * 32);
    for(int i = 0; i < 32; ++i) {
        images[i] = (double *) malloc(sizeof(double) * dim[0]);
        labels[i] = (double *) malloc(sizeof(double) * dim[end_]);
    }	

    read_bst_images("/share/data/bs/bs_train_data.dat",
                    &images,
                    &labels,
                    32,
                    16);

    rr = 0;
    printf("Start training!\n");
    for (r = 0; r < epochs; ++r) {
        for (k = 0; k < 500; ++k) {
            idx = (int) pcg32_boundedrand_r(&rng, 32);
            memcpy(lr[0].x, images[idx], sizeof(double) * dim[0]);
            memcpy(lr[end_].x, labels[idx], sizeof(double) * dim[end_]);
            backward_pass(lr, dt, gamma, L, sim_time);
            forward_pass(lr, dt, gamma, L, sim_time);
            update_weights(lr, gain, L);
        } 

        if (r % step == 0) {
            tmp_lr = lr;
            acc = 0;
            for (e = 0; e < 32; ++e) {
                memcpy(tmp_lr[0].x, images[e], sizeof(double) * dim[0]);
                for (l = 1; l < L; ++l)
                    memset(tmp_lr[l].x, 0, sizeof(double) * dim[l]);
                forward_pass(tmp_lr, dt, gamma, L, sim_time);
                m_o = argmax(labels[e], dim[end_]);
                m_y = argmax(tmp_lr[end_].x, dim[end_]);
                if (m_o == m_y)
                     acc += 1;
                memcpy(res_o+(e*dim[end_]), labels[e], sizeof(double) * dim[end_]);
                memcpy(res_y+(e*dim[end_]), tmp_lr[end_].x, sizeof(double) * dim[end_]);
            }
            error[rr] = mse(res_o, res_y, 32*dim[end_]);
            accur[rr] = (double) acc / 32;
            printf("Epoch: %d  Error: %lf  Accuracy: %lf\n", r, error[rr], accur[rr]);
            rr++;
        }
    }
    printf("Training done!\n");

    write2file("./data/analysis/analysis_rchl_final_w1.dat",
               lr[1].W, dim[1] * dim[0]);
    write2file("./data/analysis/analysis_rchl_final_w2.dat",
               lr[2].W, dim[1] * dim[2]);

    /* write2file("./data/bars_stripes/random_bias_error_bs_uniform.dat", error, size); */
    /* write2file("./data/bars_stripes/random_bias_accur_bs_uniform.dat", accur, size); */
    /* base = concatenate("./data/bars_stripes/w1_gamma_small_bs_", */
    /*                    (double) gamma_var); */
    /* write2file(base, lr[1].W, dim[1] * dim[0]); */
    /* dealloc(base); */
    /* base = concatenate("./data/bars_stripes/w2_gamma_small_bs_", */
    /*                    (double) gamma_var); */
    /* write2file(base, lr[2].W, dim[2] * dim[1]); */
    /* dealloc(base); */
    
#if RANDOM == 1
    /* base = concatenate("./data/bars_stripes/g_init_gamma_small_bs_", */
    /*                    (double) gamma_var); */
    /* write2file(base, lr[2].G, lr[1].size * lr[2].size); */
    /* dealloc(base); */
#endif
    /* base = concatenate(fname, (double) gamma); */
    /* write2file(base, error, size); */
    /* dealloc(base); */
    /* base = concatenate(fname, (double) gamma); */
    /* write2file(base, accur, size); */
    /* dealloc(base); */
    /* char err[8]="_error_", ac[8]="_accur_"; */
    /* base = get_name(fname, err, iter); */
    /* write2file(base, error, epochs); */
    /* dealloc(base); */
    /* base = get_name(fname, ac, iter); */
    /* write2file(base, accur, epochs); */
    /* dealloc(base); */

    deallocate(lr, L);
    dealloc(lr);
    for(e = 0; e < 32; ++e) {
        free(images[e]);
        free(labels[e]);
    }
    free(images);
    free(labels);
    dealloc(res_o);
    dealloc(res_y);
}


int main(int argc, char **argv)
{
    if (argc != 3) {
        printf("Invalid number of arguments!\n");
        exit(-1);
    }

    double min_or_mean=-.6, max_or_sigma=.6;
    int epochs = 15;
    long long int seed = (long long int) atoi(argv[1]);
    int iter = atoi(argv[2]);
    /* char *fname = "./data/bars_stripes/barstripes_rchl_zero_sigma"; */
    char *fname = "./data/analysis/analysis_rchl";
    double gamma=0.05, eta=0.1;
    printf("---------------------------------\n");
    run_bars_stipes(gamma, eta, min_or_mean, max_or_sigma, seed, epochs,
                    iter, fname);
    printf("---------------------------------\n");
    return 0;
}
