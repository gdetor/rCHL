# rCHL - This is an implementation of random Contrastive Hebbian Learning
# algorithm given in: Detorakis, Georgios, Travis Bartley, and Emre Neftci,
# "Contrastive Hebbian Learning with Random Feedback Weights",
# arXiv preprint arXiv:1806.07406 (2018).
#
# Copyright (C) 2018  Georgios Is. Detorakis (gdetor@pm.me)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
import numpy as np
import matplotlib.pylab as plt
from struct import unpack


def read_from_file(fname):
    with open(fname, 'rb') as f:
        c = f.read()
    size = int(len(c) // 8)
    return np.array(unpack('d' * size, c), 'd')


def plot_deg():
    epochs = 300
    err, acc = [], []
    for i in range(10):
        fname_err = './data/bars_stripes/barstripes_rchl_zero_sigma_error_'+str(i)+'.dat'
        fname_acc = './data/bars_stripes/barstripes_rchl_zero_sigma_accur_'+str(i)+'.dat'
        err.append(read_from_file(fname_err))
        acc.append(read_from_file(fname_acc))
    err = np.array(err)
    acc = np.array(acc)
    mu_re, std_re = np.mean(err, axis=0), np.std(err, axis=0)
    mu_ra, std_ra = np.mean(acc, axis=0), np.std(acc, axis=0)

    fig = plt.figure(figsize=(12, 4))
    fig.subplots_adjust(wspace=0.3, hspace=0.3, bottom=0.18)
    ax = fig.add_subplot(121)
    ax.plot(mu_re, color='k', lw=2)
    ax.fill_between(np.arange(epochs), mu_re+std_re, mu_re-std_re,
                    facecolor='k', alpha=0.2,
                    zorder=0)
    ax.set_ylim([0, 0.5])
    ax.set_xlim([0, epochs])
    ax.set_xlabel('Epochs', fontsize=18, weight='bold')
    ax.set_ylabel('Test MSE', fontsize=18, weight='bold')
    ticks = ax.get_xticks().astype('i')
    ax.set_xticklabels(ticks, fontsize=16, weight='bold')
    ticks = np.round(ax.get_yticks(), 1)
    ax.set_yticklabels(ticks, fontsize=16, weight='bold')
    ax.grid()
    ax.text(0, 0.55, 'A',
            va='top',
            ha='left',
            fontsize=22,
            weight='bold')

    ax = fig.add_subplot(122)
    ax.plot(mu_ra, color='k', lw=2)
    ax.fill_between(np.arange(epochs), mu_ra+std_ra, mu_ra-std_ra,
                    facecolor='k', alpha=0.2,
                    zorder=0)
    ax.set_ylim([0, 1])
    ax.set_xlim([0, epochs])
    ax.set_xlabel('Epochs', fontsize=18, weight='bold')
    ax.set_ylabel('Test Accuracy', fontsize=18, weight='bold')
    # ticks = np.arange(0, epochs+100, 100).astype('i')
    ticks = ax.get_xticks().astype('i')
    ax.set_xticklabels(ticks, fontsize=16, weight='bold')
    ticks = np.round(ax.get_yticks(), 1)
    ax.set_yticklabels(ticks, fontsize=16, weight='bold')
    ax.grid()
    ax.text(0, 1.1, 'B',
            va='top',
            ha='left',
            fontsize=22,
            weight='bold')
    plt.savefig("Figure20.pdf", axis='tight')


if __name__ == '__main__':
    plot_deg()
    plt.show()
