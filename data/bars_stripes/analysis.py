# rCHL - This is an implementation of random Contrastive Hebbian Learning
# algorithm given in: Detorakis, Georgios, Travis Bartley, and Emre Neftci,
# "Contrastive Hebbian Learning with Random Feedback Weights",
# arXiv preprint arXiv:1806.07406 (2018).
#
# Copyright (C) 2018  Georgios Is. Detorakis (gdetor@pm.me)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
import numpy as np
import matplotlib.pylab as plt
# from scipy.integrate import odeint
from rectpsa import rectpsa


def read_from_file(fname):
    from struct import unpack
    with open(fname, "rb") as f:
        c = f.read()
    size = int(len(c) // 8)
    return np.array(unpack('d'*size, c))


def f(x):
    return 1.0 / (1.0 + np.exp(-x))


def rhs_free(X, T, w1, x0, w2, g, gamma):
    x1 = X[:50]
    x2 = X[50:]

    dx1 = -x1 + f(np.dot(x0, w1) + gamma * np.dot(x2, g.T))
    dx2 = -x2 + f(np.dot(x1, w2))
    dx = np.hstack([dx1, dx2])
    return dx


def euler(x0, w1, w2, g, b1, b2, gamma=0.05, tf=30.0, dt=0.08):
    sim_time = int(tf / dt)

    x1 = np.zeros((sim_time, 50))
    x2 = np.zeros((sim_time, 2))

    trace = []
    for t in range(1, sim_time):
        trace.append(gamma * np.dot(x2[t-1], g.T))
        x1[t] = x1[t-1] + dt * (-x1[t-1] + f(np.dot(x0, w1)
                                             + gamma * np.dot(x2[t-1], g.T)
                                             + b1))
        x2[t] = x2[t-1] + dt * (-x2[t-1] + f(np.dot(x1[t-1], w2) + b2))
    return np.hstack([x1, x2]), np.array(trace)


def simple_analysis():
    np.random.seed(100)

    W1 = read_from_file("./data/analysis/analysis_rchl_final_w1.dat")
    W1 = W1.reshape(16, 50)
    W2 = read_from_file("./data/analysis/analysis_rchl_final_w2.dat")
    W2 = W2.reshape(50, 2)
    G = read_from_file("./data/analysis/analysis_rchl_fdbk_matrix.dat")
    G = G.reshape(50, 2)

    D = read_from_file('/share/data/bs/bs_train_data.dat').reshape(32, 16)

    # t = np.linspace(0, 30, 1000)

    fig = plt.figure(figsize=(7, 13))

    case = [0.01, .5, 4]

    npts = 20
    xmin, xmax = -5, 5
    ymin, ymax = -5, 5
    X, Y = np.meshgrid(np.linspace(xmin, xmax, npts),
                       np.linspace(ymin, ymax, npts))

    k = 1
    for i in range(3):
        # x1 = np.zeros((50,))
        # x2 = np.zeros((2,))
        x_0 = D[3]
        # x0 = np.hstack([x1, x2])
        # G = np.random.uniform(-case[i-1], case[i-1], (50, 2))
        G = np.random.normal(0, case[i], (50, 2))
        # sol_g = odeint(rhs_free, x0, t, (W1, x_0, W2, G, 0.05))
        # sol_w = odeint(rhs_free, x0, t, (W1, x_0, W2, W2, 0.05))
        sol_g, tr_g = euler(x_0, W1, W2, G)
        sol_w, tr_w = euler(x_0, W1, W2, W2)

        ax = fig.add_subplot(3, 4, 2*i+(k))
        ax.plot(np.arange(50), sol_g[:, :50].sum(axis=0), 'k', zorder=10)
        ax.plot(np.arange(50), sol_w[:, :50].sum(axis=0), 'r', zorder=0)
        # ax.set_ylim([0, 1])

        ax = fig.add_subplot(3, 4, 2*i+(k+1))
        ax.stem(np.arange(2), sol_g[:, 50:].sum(axis=0), 'k')
        ax.stem(np.arange(2), sol_w[:, 50:].sum(axis=0), 'r')

        ax = fig.add_subplot(3, 4, 2*i+(k+2))
        ax.plot(tr_g, 'k', zorder=10)
        ax.plot(tr_w, 'r', zorder=0)
        ax.set_title(str(case[i]))

        ax = fig.add_subplot(3, 4, 2*i+(k+3))
        P = np.hstack([W2, G])
        s, _, _, _, _ = rectpsa(P, X, Y, method='svd')
        ax.contour(X, Y, np.log10(s))
        ax.set_xlim([-5, 5])
        ax.set_ylim([-5, 5])
        k += 2


def analysis():
    base = './data/bars_stripes/evo/ae_chl_'
    epochs = 20

    npts = 20
    xmin, xmax = -5, 5
    ymin, ymax = -5, 5
    X, Y = np.meshgrid(np.linspace(xmin, xmax, npts),
                       np.linspace(ymin, ymax, npts))

    W1 = []
    for i in range(epochs):
        W1.append(read_from_file(base+"w_"+str(i)+"_1.dat").reshape(16, 50))

    W2 = []
    for i in range(epochs):
        W2.append(read_from_file(base+"w_"+str(i)+"_2.dat").reshape(50, 2))

    b1 = []
    for i in range(epochs):
        b1.append(read_from_file(base+"b_"+str(i)+"_1.dat"))

    b2 = []
    for i in range(epochs):
        b2.append(read_from_file(base+"b_"+str(i)+"_2.dat"))

    D = read_from_file('/share/data/bs/bs_train_data.dat').reshape(32, 16)
    x_0 = D[3]

    sigma = [0.01, 0.5, 6.0]
    sol, tr = [], []
    sol_g, tr_g = [], []
    pick_up_times = [0, 1, 19]
    s_w1 = []
    s_w2 = []
    s_g = []
    for k, j in enumerate(pick_up_times):
        s, _, _, _, _ = rectpsa(W1[j].T, X, Y, method='svd')
        s_w1.append(s)
        s, _, _, _, _ = rectpsa(W2[j], X, Y, method='svd')
        s_w2.append(s)
        sol_, tr_ = euler(x_0, W1[j], W2[j], W2[j], b1[j], b2[j])
        sol.append(sol_)
        tr.append(tr_)
        # G = np.random.uniform(-sigma[k], sigma[k], (50, 2))
        G = np.random.normal(0, sigma[k], (50, 2))
        E = np.random.normal(0, 0.0001, (50, 2))
        u, s, v = np.linalg.svd(E)
        print(np.linalg.norm(E, 2), s.max())
        s, _, _, _, _ = rectpsa(G, X, Y, method='svd')
        s_g.append(s)
        for i in pick_up_times:
            sol_, tr_ = euler(x_0, W1[i], W2[i], G, b1[i], b2[i], gamma=0.05)
            sol_g.append(sol_)
            tr_g.append(tr_)

    fig = plt.figure()
    for i in range(1, 4):
        ax = fig.add_subplot(4, 3, i)
        ax.plot(sol[i-1][:, :50], 'k')
        ax.set_ylim([0, 1])
    for j in range(9):
        ax = fig.add_subplot(4, 3, 4+j)
        ax.plot(sol_g[j], 'k')
        ax.set_ylim([0, 1])

    fig = plt.figure()
    for i in range(1, 4):
        ax = fig.add_subplot(4, 3, i)
        ax.plot(tr[i-1][:, :50], 'k')
        ax.set_ylim([-0.15, 0.15])
    for j in range(9):
        ax = fig.add_subplot(4, 3, 4+j)
        ax.plot(tr_g[j], 'k')
        ax.set_ylim([-0.15, 0.15])
    plt.title("Feedback term")

    fig = plt.figure()
    for i in range(3):
        ax = fig.add_subplot(3, 3, i+1)
        cs = ax.contour(X, Y, np.log10(s_w1[i]))
    for i in range(3):
        ax = fig.add_subplot(3, 3, 4+i)
        cs = ax.contour(X, Y, np.log10(s_w2[i]))
    for i in range(3):
        ax = fig.add_subplot(3, 3, 7+i)
        cs = ax.contour(X, Y, np.log10(s_g[i]))


if __name__ == '__main__':
    # simple_analysis()
    analysis()
    plt.show()
