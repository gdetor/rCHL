# rCHL - This is an implementation of random Contrastive Hebbian Learning
# algorithm given in: Detorakis, Georgios, Travis Bartley, and Emre Neftci,
# "Contrastive Hebbian Learning with Random Feedback Weights",
# arXiv preprint arXiv:1806.07406 (2018).
#
# Copyright (C) 2018  Georgios Is. Detorakis (gdetor@pm.me)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
import numpy as np
import matplotlib.pylab as plt
from struct import unpack
import matplotlib.gridspec as gridspec


def read_from_file(fname):
    with open(fname, 'rb') as f:
        c = f.read()
    size = int(len(c) // 8)
    return np.array(unpack('d' * size, c), 'd')


def plot_basic_res():
    err_nobias = read_from_file('./data/bars_stripes/normal_nobias_error_bs.dat')
    err_bias = read_from_file('./data/bars_stripes/normal_bias_error_bs.dat')
    err_rnobias = read_from_file('./data/bars_stripes/random_nobias_error_bs.dat')
    err_rbias = read_from_file('./data/bars_stripes/random_bias_error_bs.dat')

    acc_nobias = read_from_file('./data/bars_stripes/normal_nobias_accur_bs.dat')
    acc_bias = read_from_file('./data/bars_stripes/normal_bias_accur_bs.dat')
    acc_rnobias = read_from_file('./data/bars_stripes/random_nobias_accur_bs.dat')
    acc_rbias = read_from_file('./data/bars_stripes/random_bias_accur_bs.dat')

    bs_data = read_from_file('/share/data/bs/bs_train_data.dat').reshape(32, 16)

    error = [err_nobias, err_bias, err_rnobias, err_rbias]
    accur = [acc_nobias, acc_bias, acc_rnobias, acc_rbias]

    import matplotlib.colors as colors
    import matplotlib.cm as cmx
    vals = [1, 2, 3, 4]
    values = range(len(vals))
    jet = plt.get_cmap('BrBG')
    cNorm = colors.Normalize(vmin=0, vmax=values[-1])
    scalarMap = cmx.ScalarMappable(norm=cNorm, cmap=jet)

    col = ['k', 'b', 'm', 'c']
    lst = ['-', ':', '--', '-.']

    labels = ['CHL_NoBias', 'CHL_Bias', 'rCHL_NoBias', 'rCHL_Bias']

    plt.figure(figsize=(15, 4))
    # fig.subplots_adjust(wspace=0.3, hspace=0.3, bottom=0.2)
    gs1 = gridspec.GridSpec(1, 3)
    gs1.update(wspace=0.5, hspace=0.5, bottom=0.2)

    im_size_x, im_size_y = 4, 4
    # R = np.zeros((4*4, 4*8))
    R = np.zeros((4*4, 4*8))
    ii = 0
    for i in range(4):
        for j in range(8):
            tmp = bs_data[ii, :].reshape(im_size_x, im_size_y)
            R[i*im_size_x:(i+1)*im_size_x, j*im_size_y:(j+1)*im_size_y] = tmp
            ii += 1
            if ii > 31:
                break

    # ax = fig.add_subplot(131)
    ax = plt.subplot(gs1[0])
    ax.imshow(R, interpolation='nearest', cmap=plt.cm.gray_r, aspect='equal',
              origin='upper')
    ax.spines['right'].set_visible(False)
    ax.spines['right'].set_color(None)
    ax.spines['top'].set_visible(False)
    ax.spines['top'].set_color(None)
    ax.spines['left'].set_color(None)
    ax.spines['left'].set_visible(False)
    ax.spines['bottom'].set_visible(False)
    ax.spines['bottom'].set_color(None)
    ax.set_xticks(np.arange(-0.5, im_size_y*8-.35, im_size_y))
    ax.set_yticks(np.arange(-0.5, im_size_x*4-.35, im_size_x))
    ax.grid(color='r', linestyle='-', linewidth=1.7)
    ax.xaxis.set_tick_params(width=0, length=0, direction='in')
    ax.yaxis.set_tick_params(width=0, length=0, direction='in')
    ax.set_xticklabels([])
    ax.set_yticklabels([])
    ax.text(0, -3.7, 'A',
            va='top',
            ha='left',
            fontsize=22,
            weight='bold')
    # ax.text(0, -3.1, 'A',
    #         va='top',
    #         ha='left',
    #         fontsize=22,
    #         weight='bold')

    # ax = fig.add_subplot(132)
    ax = plt.subplot2grid((2, 7), (0, 2), colspan=2, rowspan=2)
    ax = plt.subplot(gs1[1])
    # for i in range(len(error)):
    #     ax.plot(error[i], c=col[i], lw=2, label=labels[i], ls=lst[i])
    lines = []
    for i in range(len(error)):
        line = error[i]
        colorVal = scalarMap.to_rgba(values[i])
        retLine, = ax.plot(line, color=colorVal, lw=2)
        lines.append(retLine)
    ax.set_ylim([0, 0.5])
    ax.set_xlim([0, 40])
    ax.set_xlabel('Epochs', fontsize=18, weight='bold')
    ax.set_ylabel('Test MSE', fontsize=18, weight='bold')
    ticks = ax.get_xticks().astype('i')
    ax.set_xticklabels(ticks, fontsize=16, weight='bold')
    ticks = np.round(ax.get_yticks(), 1)
    ax.set_yticklabels(ticks, fontsize=16, weight='bold')
    ax.legend()
    ax.grid()
    ax.text(0, 0.55, 'B',
            va='top',
            ha='left',
            fontsize=22,
            weight='bold')

    # ax = fig.add_subplot(133)
    ax = plt.subplot(gs1[2])
    # for i in range(len(error)):
    #     ax.plot(accur[i], c=col[i], lw=2, label=labels[i], ls=lst[i])
    lines = []
    for i in range(len(accur)):
        line = accur[i]
        colorVal = scalarMap.to_rgba(values[i])
        retLine, = ax.plot(line, color=colorVal, lw=2, label=labels[i])
        lines.append(retLine)
    ax.set_ylim([0, 1])
    ax.set_xlim([0, 40])
    ax.set_xlabel('Epochs', fontsize=18, weight='bold')
    ax.set_ylabel('Test Accuracy', fontsize=18, weight='bold')
    ticks = ax.get_xticks().astype('i')
    ax.set_xticklabels(ticks, fontsize=16, weight='bold')
    ticks = np.round(ax.get_yticks(), 1)
    ax.set_yticklabels(ticks, fontsize=16, weight='bold')
    ax.legend()
    ax.grid()
    ax.text(0, 1.1, 'C',
            va='top',
            ha='left',
            fontsize=22,
            weight='bold')

    plt.savefig('Figure02.pdf', axis='tight')


def plot_feedback_gain():
    # gamma = [0.01, 0.02, 0.03, 0.04, 0.05, 0.09, 0.1, 0.5, 1.0]
    gamma = [0.0, 0.0001, 0.001, 0.01, 0.05, 0.1, 0.5, 1.0, 1.5]

    error, accur = [], []
    for j in range(len(gamma)):
        error.append(read_from_file('./data/bars_stripes/feedback_1_rchl_error_'
                                    + str(j) + '.dat'))
        accur.append(read_from_file('./data/bars_stripes/feedback_1_rchl_accur_'
                                    + str(j) + '.dat'))
        # error.append(read_from_file('./data/bars_stripes/feedback_error_bs_'
        #                             + '%.6f' % i + '.dat'))
        # accur.append(read_from_file('./data/bars_stripes/feedback_accur_bs_'
        #                             + '%.6f' % i + '.dat'))

    import matplotlib.colors as colors
    import matplotlib.cm as cmx
    values = range(len(gamma))
    # jet = plt.get_cmap('Set1')
    jet = plt.get_cmap('BrBG')
    cNorm = colors.Normalize(vmin=0, vmax=values[-1])
    scalarMap = cmx.ScalarMappable(norm=cNorm, cmap=jet)

    fig = plt.figure(figsize=(13.5, 5))
    fig.subplots_adjust(wspace=0.3, hspace=0.3, bottom=0.18)
    ax = fig.add_subplot(121)

    lines = []
    for i in range(len(error)):
        line = error[i]
        colorVal = scalarMap.to_rgba(values[i])
        if i == 4:
            colorVal = (0.5, 0.5, 0.5)
        retLine, = ax.plot(line, color=colorVal, lw=2)
        lines.append(retLine)
    ax.set_ylim([0, 0.6])
    ax.set_xlim([0, 40])

    ax.set_xlabel('Epochs', fontsize=18, weight='bold')
    ax.set_ylabel('Test MSE', fontsize=18, weight='bold')
    ticks = ax.get_xticks().astype('i')
    ax.set_xticklabels(ticks, fontsize=16, weight='bold')
    ticks = np.round(ax.get_yticks(), 1)
    ax.set_yticklabels(ticks, fontsize=16, weight='bold')
    ax.grid()
    ax.text(0, 0.66, 'A',
            va='top',
            ha='left',
            fontsize=22,
            weight='bold')

    ax = fig.add_subplot(122)
    lines = []
    for i in range(len(accur)):
        line = accur[i]
        colorVal = scalarMap.to_rgba(values[i])
        if i == 4:
            colorVal = (0.5, 0.5, 0.5)
        retLine, = ax.plot(line, color=colorVal, lw=2,
                           label=r'$\gamma = $'+str(gamma[i]))
        lines.append(retLine)
    ax.set_ylim([0, 1])
    ax.set_xlim([0, 40])

    ax.set_xlabel('Epochs', fontsize=18, weight='bold')
    ax.set_ylabel('Test Accuracy', fontsize=18, weight='bold')
    ticks = ax.get_xticks().astype('i')
    ax.set_xticklabels(ticks, fontsize=16, weight='bold')
    ticks = ax.get_yticks()
    ax.set_yticklabels(np.round(ticks, 1), fontsize=16, weight='bold')
    ax.grid()
    ax.legend(loc=3, bbox_to_anchor=(1.0, 0.2), ncol=1)
    ax.text(0, 1.09, 'B',
            va='top',
            ha='left',
            fontsize=22,
            weight='bold')

    plt.savefig('Figure31.pdf', axis='tight')


def plot_feedback_test():
    import matplotlib.colors as colors
    import matplotlib.cm as cmx

    base = './data/bars_stripes/'
    err = []
    err.append(read_from_file(base+'feedback_test_chl_error_0.dat'))
    err.append(read_from_file(base+'feedback_test_chl_error_1.dat'))
    err.append(read_from_file(base+'feedback_test_chl_error_2.dat'))
    err.append(read_from_file(base+'feedback_test_rchl_error_0.dat'))
    err.append(read_from_file(base+'feedback_test_rchl_error_1.dat'))
    err.append(read_from_file(base+'feedback_test_rchl_error_2.dat'))
    err.append(read_from_file(base+'feedback_test_rchl_error_3.dat'))

    acc = []
    acc.append(read_from_file(base+'feedback_test_chl_accur_0.dat'))
    acc.append(read_from_file(base+'feedback_test_chl_accur_1.dat'))
    acc.append(read_from_file(base+'feedback_test_chl_accur_2.dat'))
    acc.append(read_from_file(base+'feedback_test_rchl_accur_0.dat'))
    acc.append(read_from_file(base+'feedback_test_rchl_accur_1.dat'))
    acc.append(read_from_file(base+'feedback_test_rchl_accur_2.dat'))
    acc.append(read_from_file(base+'feedback_test_rchl_accur_3.dat'))

    vals = [1, 2, 3, 4, 5, 6, 7]
    values = range(len(vals))
    jet = plt.get_cmap('BrBG')
    cNorm = colors.Normalize(vmin=0, vmax=values[-1])
    scalarMap = cmx.ScalarMappable(norm=cNorm, cmap=jet)

    labels = ['CHL-4-Hidden', 'CHL-3-Hidden', 'CHL-2-Hidden',
              'rCHL-4-Hidden', 'rCHL-3-Hidden', 'rCHL-2-Hidden',
              'rCHL-4-Hidden-G']

    fig = plt.figure(figsize=(12, 5))
    fig.subplots_adjust(wspace=0.3, hspace=0.3, bottom=0.2)

    ax = fig.add_subplot(121)
    lines = []
    for i in range(len(err)):
        line = err[i]
        colorVal = scalarMap.to_rgba(values[i])
        if i == 3:
            colorVal = (0.5, 0.5, 0.5)
        retLine, = ax.plot(line, color=colorVal, lw=2)
        lines.append(retLine)
    ax.set_ylim([0, 0.5])
    ax.set_xlim([0, 100])
    ax.set_xlabel('Epochs', fontsize=18, weight='bold')
    ax.set_ylabel('Test MSE', fontsize=18, weight='bold')
    ticks = ax.get_xticks().astype('i')
    ax.set_xticklabels(ticks, fontsize=16, weight='bold')
    ticks = np.round(ax.get_yticks(), 1)
    ax.set_yticklabels(ticks, fontsize=16, weight='bold')
    ax.legend()
    ax.grid()
    ax.text(0, 0.55, 'A',
            va='top',
            ha='left',
            fontsize=22,
            weight='bold')

    ax = fig.add_subplot(122)
    lines = []
    for i in range(len(acc)):
        line = acc[i]
        colorVal = scalarMap.to_rgba(values[i])
        if i == 3:
            colorVal = (0.5, 0.5, 0.5)
        retLine, = ax.plot(line, color=colorVal, lw=2, label=labels[i])
        lines.append(retLine)
    ax.set_ylim([0, 1])
    ax.set_xlim([0, 100])
    ax.set_xlabel('Epochs', fontsize=18, weight='bold')
    ax.set_ylabel('Test Accuracy', fontsize=18, weight='bold')
    ticks = ax.get_xticks().astype('i')
    ax.set_xticklabels(ticks, fontsize=16, weight='bold')
    ticks = np.round(ax.get_yticks(), 1)
    ax.set_yticklabels(ticks, fontsize=16, weight='bold')
    ax.legend()
    ax.grid()
    ax.text(0, 1.1, 'B',
            va='top',
            ha='left',
            fontsize=22,
            weight='bold')

    plt.savefig('Figure32.pdf', axis='tight')


def plot_learning_rate():
    eta = [0.001, 0.005, 0.01, 0.05, 0.1, 0.5]

    error, accur = [], []
    for j, i in enumerate(eta):
        error.append(read_from_file('./data/bars_stripes/learningrate_error_bs_'
                                    + '%.6f' % i + '.dat'))
        accur.append(read_from_file('./data/bars_stripes/learningrate_accur_bs_'
                                    + '%.6f' % i + '.dat'))

    import matplotlib.colors as colors
    import matplotlib.cm as cmx
    values = range(len(eta))
    jet = plt.get_cmap('BrBG')
    cNorm = colors.Normalize(vmin=0, vmax=values[-1])
    scalarMap = cmx.ScalarMappable(norm=cNorm, cmap=jet)

    fig = plt.figure(figsize=(12, 4))
    fig.subplots_adjust(wspace=0.3, hspace=0.3, bottom=0.18, right=0.85)
    ax = fig.add_subplot(121)

    lines = []
    for i in range(len(error)):
        line = error[i]
        colorVal = scalarMap.to_rgba(values[i])
        retLine, = ax.plot(line, color=colorVal, lw=2)
        lines.append(retLine)
    ax.set_ylim([0, 0.6])
    ax.set_xlim([0, 200])

    ax.set_xlabel('Epochs', fontsize=18, weight='bold')
    ax.set_ylabel('Test MSE', fontsize=18, weight='bold')
    ax.set_xticks([0, 50, 100, 150, 200])
    ticks = ax.get_xticks().astype('i')
    ax.set_xticklabels(ticks, fontsize=16, weight='bold')
    ticks = np.round(ax.get_yticks(), 1)
    ax.set_yticklabels(ticks, fontsize=16, weight='bold')
    ax.grid()
    ax.text(0, 0.66, 'A',
            va='top',
            ha='left',
            fontsize=22,
            weight='bold')

    ax = fig.add_subplot(122)
    lines = []
    for i in range(len(accur)):
        line = accur[i]
        colorVal = scalarMap.to_rgba(values[i])
        retLine, = ax.plot(line, color=colorVal, lw=2,
                           label=r'$\eta = $'+str(eta[i]))
        lines.append(retLine)
    ax.set_ylim([0, 1])
    ax.set_xlim([0, 200])

    ax.set_xlabel('Epochs', fontsize=18, weight='bold')
    ax.set_ylabel('Test Accuracy', fontsize=18, weight='bold')
    ax.set_xticks([0, 50, 100, 150, 200])
    ticks = ax.get_xticks().astype('i')
    ax.set_xticklabels(ticks, fontsize=16, weight='bold')
    ticks = np.round(ax.get_yticks(), 1)
    ax.set_yticklabels(ticks, fontsize=16, weight='bold')
    ax.grid()
    ax.legend(loc=3, bbox_to_anchor=(1.0, 0.2), ncol=1)
    ax.text(0, 1.09, 'B',
            va='top',
            ha='left',
            fontsize=22,
            weight='bold')

    plt.savefig('Figure19.pdf', axis='tight')


def plot_layers():
    var = [3, 4, 5, 6]

    error, accur = [], []
    for j, i in enumerate(var):
        error.append(read_from_file('./data/bars_stripes/layers_error_bs_'
                                    + '%.6f' % i + '.dat'))
        accur.append(read_from_file('./data/bars_stripes/layers_accur_bs_'
                                    + '%.6f' % i + '.dat'))

    import matplotlib.colors as colors
    import matplotlib.cm as cmx
    values = range(len(var))
    jet = plt.get_cmap('BrBG')
    cNorm = colors.Normalize(vmin=0, vmax=values[-1])
    scalarMap = cmx.ScalarMappable(norm=cNorm, cmap=jet)

    fig = plt.figure(figsize=(12, 4))
    fig.subplots_adjust(wspace=0.3, hspace=0.3, bottom=.2, right=0.85)
    ax = fig.add_subplot(121)

    lines = []
    for i in range(len(error)):
        line = error[i]
        colorVal = scalarMap.to_rgba(values[i])
        retLine, = ax.plot(line, color=colorVal, lw=2)
        lines.append(retLine)
    ax.set_ylim([0, 0.6])
    ax.set_xlim([0, 120])

    ax.set_xlabel('Epochs', fontsize=18, weight='bold')
    ax.set_ylabel('Test MSE', fontsize=18, weight='bold')
    ticks = ax.get_xticks().astype('i')
    ax.set_xticklabels(ticks, fontsize=16, weight='bold')
    ticks = np.round(ax.get_yticks(), 1)
    ax.set_yticklabels(ticks, fontsize=16, weight='bold')
    ax.grid()
    ax.text(0, 0.66, 'A',
            va='top',
            ha='left',
            fontsize=22,
            weight='bold')

    ax = fig.add_subplot(122)
    lines = []
    for i in range(len(accur)):
        line = accur[i]
        colorVal = scalarMap.to_rgba(values[i])
        if i < 3:
            retLine, = ax.plot(line, color=colorVal, lw=2,
                               label=r'$L = $'+str(var[i]))
        else:
            retLine, = ax.plot(line, color=colorVal, lw=2,
                               label=r'$L = $'+str(var[i])+r',$\gamma=0.1$')
        lines.append(retLine)
    ax.set_ylim([0, 1])
    ax.set_xlim([0, 120])

    ax.set_xlabel('Epochs', fontsize=18, weight='bold')
    ax.set_ylabel('Test Accuracy', fontsize=18, weight='bold')
    ticks = ax.get_xticks().astype('i')
    ax.set_xticklabels(ticks, fontsize=16, weight='bold')
    ticks = np.round(ax.get_yticks(), 1)
    ax.set_yticklabels(ticks, fontsize=16, weight='bold')
    ax.grid()
    ax.legend(loc=3, bbox_to_anchor=(1.0, 0.6), ncol=1)
    ax.text(0, 1.1, 'B',
            va='top',
            ha='left',
            fontsize=22,
            weight='bold')

    plt.savefig('Figure04.pdf', axis='tight')


def plot_distribution():
    # var = [0.01, 0.02, 0.03, 0.04, 0.05, 0.09, 0.1, 0.2, 0.3, 0.4, 0.5,
    #        0.6, 0.7, 0.8, 0.9, 1.0]
    var = [0.01, 0.05, 0.5, 0.8, 1.0]

    error_g, accur_g = [], []
    error_u, accur_u = [], []
    for j, i in enumerate(var):
        error_g.append(read_from_file('./data/bars_stripes/gaussian_error_bs_'
                                      + '%.6f' % i + '.dat'))
        accur_g.append(read_from_file('./data/bars_stripes/gaussian_accur_bs_'
                                      + '%.6f' % i + '.dat'))
        error_u.append(read_from_file('./data/bars_stripes/uniform_error_bs_'
                                      + '%.6f' % i + '.dat'))
        accur_u.append(read_from_file('./data/bars_stripes/uniform_accur_bs_'
                                      + '%.6f' % i + '.dat'))

    import matplotlib.colors as colors
    import matplotlib.cm as cmx
    values = range(len(var))
    jet = plt.get_cmap('BrBG')
    cNorm = colors.Normalize(vmin=0, vmax=values[-1])
    scalarMap = cmx.ScalarMappable(norm=cNorm, cmap=jet)

    fig = plt.figure(figsize=(13, 7))
    fig.subplots_adjust(wspace=0.3, hspace=0.3, right=0.85)

    ax = fig.add_subplot(221)
    lines = []
    for i in range(len(error_g)):
        line = error_g[i]
        colorVal = scalarMap.to_rgba(values[i])
        retLine, = ax.plot(line, color=colorVal, lw=2)
        lines.append(retLine)
    ax.set_ylim([0, 0.6])
    ax.set_xlim([0, 120])
    ax.text(0, 0.68, 'A',
            va='top',
            ha='left',
            fontsize=22,
            weight='bold')

    # ax.set_xlabel('Epochs (x500)', fontsize=18, weight='bold')
    ax.set_ylabel('Test MSE', fontsize=18, weight='bold')
    ticks = ax.get_xticks().astype('i')
    ax.set_xticklabels(ticks, fontsize=16, weight='bold')
    ticks = np.round(ax.get_yticks(), 2)
    ax.set_yticklabels(ticks, fontsize=16, weight='bold')
    ax.grid()

    ax = fig.add_subplot(222)
    lines = []
    for i in range(len(accur_g)):
        line = accur_g[i]
        colorVal = scalarMap.to_rgba(values[i])
        retLine, = ax.plot(line, color=colorVal, lw=2,
                           label=r'$\sigma = $'+str(var[i]))
        lines.append(retLine)
    ax.set_ylim([0, 1])
    ax.set_xlim([0, 120])
    ax.text(0, 1.12, 'B',
            va='top',
            ha='left',
            fontsize=22,
            weight='bold')

    # ax.set_xlabel('Epochs (x500)', fontsize=18, weight='bold')
    ax.set_ylabel('Test Accuracy', fontsize=18, weight='bold')
    ticks = ax.get_xticks().astype('i')
    ax.set_xticklabels(ticks, fontsize=16, weight='bold')
    ticks = np.round(ax.get_yticks(), 2)
    ax.set_yticklabels(ticks, fontsize=16, weight='bold')
    ax.grid()
    ax.legend(loc=3, bbox_to_anchor=(1.05, 0.5), ncol=1)

    ax = fig.add_subplot(223)
    lines = []
    for i in range(len(error_u)):
        line = error_u[i]
        colorVal = scalarMap.to_rgba(values[i])
        retLine, = ax.plot(line, color=colorVal, lw=2)
        lines.append(retLine)
    ax.set_ylim([0, 0.6])
    ax.set_xlim([0, 120])
    ax.text(0, 0.68, 'C',
            va='top',
            ha='left',
            fontsize=22,
            weight='bold')

    ax.set_xlabel('Epochs', fontsize=18, weight='bold')
    ax.set_ylabel('Test MSE', fontsize=18, weight='bold')
    ticks = ax.get_xticks().astype('i')
    ax.set_xticklabels(ticks, fontsize=16, weight='bold')
    ticks = np.round(ax.get_yticks(), 2)
    ax.set_yticklabels(ticks, fontsize=16, weight='bold')
    ax.grid()

    ax = fig.add_subplot(224)
    lines = []
    for i in range(len(accur_u)):
        line = accur_u[i]
        colorVal = scalarMap.to_rgba(values[i])
        retLine, = ax.plot(line, color=colorVal, lw=2,
                           label=r'$\lambda = $'+str(2*var[i]))
        lines.append(retLine)
    ax.set_ylim([0, 1])
    ax.set_xlim([0, 120])
    ax.text(0, 1.12, 'D',
            va='top',
            ha='left',
            fontsize=22,
            weight='bold')

    ax.set_xlabel('Epochs', fontsize=18, weight='bold')
    ax.set_ylabel('Test Accuracy', fontsize=18, weight='bold')
    ticks = ax.get_xticks().astype('i')
    ax.set_xticklabels(ticks, fontsize=16, weight='bold')
    ticks = np.round(ax.get_yticks(), 2)
    ax.set_yticklabels(ticks, fontsize=16, weight='bold')
    ax.grid()
    ax.legend(loc=3, bbox_to_anchor=(1.05, 0.5), ncol=1)
    # ax.legend(loc=3, bbox_to_anchor=(1.0, 0.1), ncol=1)
    plt.savefig('Figure05.pdf', axis='tight')


def plot_analysis():
    from rectpsa import rectpsa
    norm = np.linalg.norm
    tran = np.linalg.pinv

    var = [0.01, 0.5, 1.0]
    m = [16, 50, 2]

    base = './data/bars_stripes/'
    g = []
    w1_init, w2_init, w1, w2 = [], [], [], []
    w1_init.append(read_from_file(base+'w1_hebb_bars_stripes_init.dat'
                                  ).reshape(m[0], m[1]).T)
    w2_init.append(read_from_file(base+'w2_hebb_bars_stripes_init.dat'
                                  ).reshape(m[1], m[2]))

    err = []
    for j, i in enumerate(var):
        err.append(read_from_file(base+'uniform_error_bs_'
                                  + '%.6f' % i + '.dat'))

    for j, i in enumerate(var):
        err.append(read_from_file(base+'gaussian_error_bs_'
                                  + '%.6f' % i + '.dat'))

    for j, i in enumerate(var):
        w1.append(read_from_file(base+'w1_hebb_bars_stripes_uniform_'
                                 + '%.6f' % i + '.dat').reshape(m[0],
                                                                m[1]).T)
        w2.append(read_from_file(base+'w2_hebb_bars_stripes_uniform_'
                                      + '%.6f' % i + '.dat').reshape(m[1],
                                                                     m[2]))
        g.append(read_from_file(base+'g_init_uniform_bs_'
                                + '%.6f' % i + '.dat').reshape(m[1], m[2]))
    for j, i in enumerate(var):
        w1.append(read_from_file(base+'w1_hebb_bars_stripes_gaussian_'
                                 + '%.6f' % i + '.dat').reshape(m[0],
                                                                m[1]).T)
        w2.append(read_from_file(base+'w2_hebb_bars_stripes_gaussian_'
                                      + '%.6f' % i + '.dat').reshape(m[1],
                                                                     m[2]))
        g.append(read_from_file(base+'g_init_gaussian_bs_'
                                + '%.6f' % i + '.dat').reshape(m[1], m[2]))

    npts = 20
    xmin, xmax = -5, 5
    ymin, ymax = -5, 5
    X, Y = np.meshgrid(np.linspace(xmin, xmax, npts),
                       np.linspace(ymin, ymax, npts))

    R1init, R2init, R1, R2 = [], [], [], []
    # Sinit, Smid, Sfinal = [], [], []
    C1init, C1, C2init, C2 = [], [], [], []
    CG, RG = [], []

    C1init.append(norm(w1_init[0], ord=2) * norm(tran(w1_init[0]), ord=2))
    s, _, _, _, _ = rectpsa(w1_init[0], X, Y, method='svd')
    R1init.append(s)

    C2init.append(norm(w2_init[0], ord=2) * norm(tran(w2_init[0]), ord=2))
    s, _, _, _, _ = rectpsa(w2_init[0], X, Y, method='svd')
    R2init.append(s)

    for i in range(len(w1)):
        C1.append(norm(w1[i], ord=2) * norm(tran(w1[i]), ord=2))
        s, _, _, _, _ = rectpsa(w1[i], X, Y, method='svd')
        R1.append(s)

        C2.append(norm(w2[i], ord=2) * norm(tran(w2[i]), ord=2))
        s, _, _, _, _ = rectpsa(w2[i], X, Y, method='svd')
        R2.append(s)

        CG.append(norm(g[i], ord=2) * norm(tran(g[i]), ord=2))
        s, _, _, _, _ = rectpsa(g[i], X, Y, method='svd')
        RG.append(s)

    fig = plt.figure(figsize=(12, 12))
    fig.subplots_adjust(wspace=0.3, hspace=0.3, left=.2)
    ax = plt.subplot2grid((7, 4), (0, 0), colspan=1, rowspan=1)
    ax.contour(X, Y, np.log10(R1init[0]))

    ticks = ax.get_yticks().astype('i')
    ax.set_yticklabels(ticks, fontsize=16, weight='bold')
    ax.set_xticks([])
    ax.set_title(r'${\bf W}_1$', fontsize=18, weight='bold')
    ax.set_ylabel(r'Initial ${\bf W}_i$', fontsize=18,
                  labelpad=50).set_rotation(0)

    ax = plt.subplot2grid((7, 4), (0, 1), colspan=1, rowspan=1)
    ax.contour(X, Y, np.log10(R2init[0]))
    ax.set_title(r'${\bf W}_2$', fontsize=18, weight='bold')
    ax.set_xticks([])
    ax.set_yticks([])

    lab = [r'$\mathcal{U}(-0.01, 0.01)$', r'$\mathcal{U}(-0.5, 0.5)$',
           r'$\mathcal{U}(-1, 1)$']
    dist = [70, 65, 50]
    for i in range(3):
        ax = plt.subplot2grid((7, 4), (1+i, 0), colspan=1, rowspan=1)
        ax.contour(X, Y, np.log10(R1[i]))
        ax.set_xticks([])
        ticks = ax.get_yticks().astype('i')
        ax.set_yticklabels(ticks, fontsize=16, weight='bold')
        ax.text(-5, 5, r'$cond({\bf W}_1)=$'+str(np.round(C1[i], 2)),
                ha='left',
                va='top',
                color='r',
                fontsize=12,
                weight='bold')
        ax.set_ylabel(lab[i], fontsize=18, labelpad=dist[i]).set_rotation(0)

        ax = plt.subplot2grid((7, 4), (1+i, 1), colspan=1, rowspan=1)
        ax.contour(X, Y, np.log10(R2[i]))
        ax.set_yticks([])
        ax.set_xticks([])
        ax.text(-5, 5, r'$cond({\bf W}_2)=$'+str(np.round(C2[i], 2)),
                ha='left',
                va='top',
                color='r',
                fontsize=12,
                weight='bold')

        ax = plt.subplot2grid((7, 4), (1+i, 2), colspan=1, rowspan=1)
        ax.contour(X, Y, np.log10(RG[i]))
        if i == 0:
            ax.set_title(r'${\bf G}_2$', fontsize=18, weight='bold')
        ax.set_yticks([])
        ax.set_xticks([])
        ax.text(-5, 5, r'$cond({\bf G}_2)=$'+str(np.round(CG[i], 2)),
                ha='left',
                va='top',
                color='r',
                fontsize=12,
                weight='bold')

    lab = [r'$\mathcal{N}(0, 0.01)$', r'$\mathcal{N}(0, 0.5)$',
           r'$\mathcal{N}(0, 1)$']
    dist = [60, 60, 50]
    for i in range(3):
        ax = plt.subplot2grid((7, 4), (4+i, 0), colspan=1, rowspan=1)
        ax.contour(X, Y, np.log10(R1[3+i]))
        ticks = ax.get_xticks().astype('i')
        ax.set_xticklabels(ticks, fontsize=16, weight='bold')
        ticks = ax.get_yticks().astype('i')
        ax.set_yticklabels(ticks, fontsize=16, weight='bold')
        ax.set_ylabel(lab[i], fontsize=18, labelpad=dist[i]).set_rotation(0)
        if i != 2:
            ax.set_xticks([])
        ax.text(-5, 5, r'$cond({\bf W}_1)=$'+str(np.round(C1[3+i], 2)),
                ha='left',
                va='top',
                color='r',
                fontsize=12,
                weight='bold')

        ax = plt.subplot2grid((7, 4), (4+i, 1), colspan=1, rowspan=1)
        ax.contour(X, Y, np.log10(R2[3+i]))
        ax.set_yticks([])
        ticks = ax.get_xticks().astype('i')
        ax.set_xticklabels(ticks, fontsize=16, weight='bold')
        if i != 2:
            ax.set_xticks([])
        ax.text(-5, 5, r'$cond({\bf W}_2)=$'+str(np.round(C2[3+i], 2)),
                ha='left',
                va='top',
                color='r',
                fontsize=12,
                weight='bold')

        ax = plt.subplot2grid((7, 4), (4+i, 2), colspan=1, rowspan=1)
        ax.contour(X, Y, np.log10(RG[3+i]))
        ax.set_yticks([])
        ticks = ax.get_xticks().astype('i')
        ax.set_xticklabels(ticks, fontsize=16, weight='bold')
        if i != 2:
            ax.set_xticks([])
        ax.text(-5, 5, r'$cond({\bf G}_2)=$'+str(np.round(CG[3+i], 2)),
                ha='left',
                va='top',
                color='r',
                fontsize=12,
                weight='bold')

        for i in range(len(err)):
            ax = plt.subplot2grid((7, 4), (1+i, 3), colspan=1, rowspan=1)
            ax.plot(err[i], 'k', lw=1.5)
            ax.set_xticks([0, 60, 120])
            ax.set_yticks([0, 0.3, 0.6])
            ticks = np.round(ax.get_yticks(), 4)
            ax.set_yticklabels(ticks, fontsize=12, weight='bold')
            if i == 0:
                ax.set_title('Test MSE', fontsize=16, weight='bold')
            if i != len(err)-1:
                ax.set_xticks([])
            if i == len(err)-1:
                ticks = ax.get_xticks().astype('i')
                ax.set_xticklabels(ticks, fontsize=14, weight='bold')
                ax.set_xlabel('Time', fontsize=16, weight='bold')
    # plt.savefig('Figure06.pdf', axis='tight')


def plot_analysis_new():
    from rectpsa import rectpsa
    from mpl_toolkits.axes_grid1.inset_locator import inset_axes
    norm = np.linalg.norm
    tran = np.linalg.pinv

    var = [0.01, 0.5, 1.0]
    m = [16, 50, 2]

    base = './data/bars_stripes/'
    g = []

    err = []
    for j, i in enumerate(var):
        err.append(read_from_file(base+'uniform_error_bs_'
                                  + '%.6f' % i + '.dat'))

    for j, i in enumerate(var):
        err.append(read_from_file(base+'gaussian_error_bs_'
                                  + '%.6f' % i + '.dat'))

    for j, i in enumerate(var):
        g.append(read_from_file(base+'g_init_uniform_bs_'
                                + '%.6f' % i + '.dat').reshape(m[1], m[2]))
    for j, i in enumerate(var):
        g.append(read_from_file(base+'g_init_gaussian_bs_'
                                + '%.6f' % i + '.dat').reshape(m[1], m[2]))

    M, K = 5, 5
    npts = 60
    xmin, xmax = -M, M
    ymin, ymax = -K, K
    X, Y = np.meshgrid(np.linspace(xmin, xmax, npts),
                       np.linspace(ymin, ymax, npts))

    CG, RG = [], []

    for i in range(len(g)):
        CG.append(norm(g[i], ord=2) * norm(tran(g[i]), ord=2))
        s, _, _, _, _ = rectpsa(g[i], X, Y, method='svd')
        U, S, V = np.linalg.svd(g[i])
        print(S.min())
        print(S.max())
        # print(np.linalg.norm(g[i]))
        RG.append(s)

    fig = plt.figure(figsize=(14, 7))
    fig.subplots_adjust(wspace=0.5, hspace=0.5)

    titleU = [r'$\mathcal{U}(-0.01, 0.01)$', r'$\mathcal{U}(-0.5, 0.5)$',
              r'$\mathcal{U}(-1, 1)$']
    titleN = [r'$\mathcal{N}(0, 0.01)$', r'$\mathcal{N}(0, 0.5)$',
              r'$\mathcal{N}(0, 1)$']
    indexU = ['A', 'B', 'C']
    indexN = ['D', 'E', 'F']
    for j in range(3):
        ax = plt.subplot2grid((4, 6), (0, j*2), colspan=2, rowspan=2)
        cs = ax.contour(X, Y, np.log10(RG[j]))
        ax.axvline(0, ls='--', color='black', lw=2)
        ax.set_xlim([-M, M])
        ax.set_ylim([-K, K])
        cbar = plt.colorbar(cs)
        cbar.ax.tick_params(labelsize=12)
        ax.set_xticks([])
        ticks = ax.get_yticks().astype('i')
        ax.set_yticklabels(ticks, fontsize=16, weight='bold')
        if j > 0:
            ax.set_yticks([])
        ax.set_title(titleU[j], fontsize=16, weight='bold')
        ax.text(-5, 6.3, indexU[j],
                ha='left',
                va='top',
                fontsize=20,
                weight='bold')
        axins = inset_axes(ax,  width="30%", height=.6, loc=1)
        axins.plot(err[j])
        axins.set_xticks([])
        axins.set_yticks([])
    for j in range(3):
        ax = plt.subplot2grid((4, 6), (2, j*2), colspan=2, rowspan=2)
        cs = ax.contour(X, Y, np.log10(RG[3+j]))
        ax.axvline(0, ls='--', color='black', lw=2)
        ax.set_xlim([-M, M])
        ax.set_ylim([-K, K])
        cbar = plt.colorbar(cs)
        cbar.ax.tick_params(labelsize=12)
        ticks = ax.get_xticks().astype('i')
        ax.set_xticklabels(ticks, fontsize=16, weight='bold')
        ticks = ax.get_yticks().astype('i')
        ax.set_yticklabels(ticks, fontsize=16, weight='bold')
        if j > 0:
            ax.set_yticks([])
        ax.set_title(titleN[j], fontsize=16, weight='bold')
        ax.text(-5, 6.3, indexN[j],
                ha='left',
                va='top',
                fontsize=20,
                weight='bold')
        axins = inset_axes(ax,  width="30%", height=.6, loc=1)
        axins.plot(err[3+j], 'k')
        axins.set_xticks([])
        axins.set_yticks([])
    plt.savefig('Figure06.pdf', axis='tight')


def plot_blindspot():
    base = './data/bars_stripes/'
    error = read_from_file(base+'learningrate_error_bs_0.500000.dat')
    accur = read_from_file(base+'learningrate_accur_bs_0.500000.dat')

    fig = plt.figure()
    ax = fig.add_subplot(121)
    ax.plot(error)

    ax = fig.add_subplot(122)
    ax.plot(accur)


if __name__ == '__main__':
    # plot_basic_res()
    # plot_feedback_gain()
    # plot_layers()
    # plot_distribution()
    # plot_analysis()
    plot_analysis_new()
    # plot_blindspot()
    # plot_learning_rate()
    # plot_feedback_test()

    plt.show()
