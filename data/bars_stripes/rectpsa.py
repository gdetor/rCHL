# rCHL - This is an implementation of random Contrastive Hebbian Learning
# algorithm given in: Detorakis, Georgios, Travis Bartley, and Emre Neftci,
# "Contrastive Hebbian Learning with Random Feedback Weights",
# arXiv preprint arXiv:1806.07406 (2018).
#
# Copyright (C) 2018  Georgios Is. Detorakis (gdetor@pm.me)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
import numpy as np
import scipy as sc
from scipy.sparse.linalg import eigsh


def rectpsa(A, X, Y, num_eigs=2, method='lanczos'):
    m, n = A.shape

    if method == 'lanczos':
        num_eigs = np.ndim(A) - 1
    else:
        num_eigs = A.shape[0]

    if X.size == 0 or Y.size == 0:
        raise "No grid points found!"

    if m >= 2*n:
        I_ = np.zeros((2*n, n))
        np.fill_diagonal(I_[:n, :], 1)
    else:
        I_ = np.zeros((m, n))
        np.fill_diagonal(I_, 1)
    T = np.zeros((m, n))

    if m >= 2*n:
        print("M >= 2N")
        S = np.zeros((2*n, n))
        Q, R = np.linalg.qr(A[n:m, :], mode='complete')
        S[:n, :] = A[:n, :]
        S[n:, :] = R[:n, :]
        T = I_
    elif m < 2*n:
        print("M < 2N")
        S = np.zeros((m, n))
        A1, A2 = A[:m-n, :], A[m-n:, :]
        I1, I2 = I_[:m-n, :], I_[m-n:, :]
        S2, T2, _, _, Q, Z = sc.linalg.ordqz(A2, I2, sort='iuc')
        S[:m-n, :] = np.dot(A1, Z)
        S[m-n:, :] = S2         # S2 = dot(dot(Q, A2), Z)
        T[:m-n, :] = np.dot(I1, Z)
        T[m-n:, :] = T2         # T2 = dot(dot(Q, I2), Z)
    else:
        print("Invalid matrix dimensions")

    # eps = 10e-1
    zz = X + 1j * Y
    sigma = np.zeros((zz.shape[0], zz.shape[1]))
    if method == "lanczos":
        for i in range(zz.shape[0]):
            for j in range(zz.shape[1]):
                tmp = zz[i, j] * T - S
                Q, R = np.linalg.qr(tmp, mode='complete')
                R = np.asmatrix(R[:n, :])
                tmp = np.dot(R.H, R)
                s, v = eigsh(tmp, k=num_eigs, ncv=4, which='SA')
                sigma[i, j] = np.sqrt(s.real)
    elif method == 'svd':
        for i in range(zz.shape[0]):
            for j in range(zz.shape[1]):
                tmp = zz[i, j] * T - S
                Q, R = np.linalg.qr(tmp, mode='complete')
                R = np.asmatrix(R[:n, :])
                tmp = np.dot(R.H, R)
                u, s, v = np.linalg.svd(tmp)
                sigma[i, j] = np.sqrt(s.min().real)
                # sigma[i, j] = 1.0 / np.sqrt(s.min()+1e-10)
    else:
        raise "No method found!"

    return sigma, Q, R, T, S


def read_from_file(fname):
    from struct import unpack
    with open(fname, "rb") as f:
        c = f.read()
    size = int(len(c) // 8)
    return np.array(unpack('d'*size, c))


if __name__ == '__main__':
    # A = np.random.randint(1, 10, (8, 4))
    A = np.array([[1, 10, 10], [0, 2.1, 4.2], [0, 0.1, 0.2], [0, 0.1, 0.2]])
    # A = A[:3, :]
    from scipy.linalg import companion
    A = companion(np.random.randint(-10, 10, (20,)))
    A = A[:, :10]
    import matplotlib.pylab as plt

    if 0:
        from scipy.linalg import eigvals
        from pseudopy import NonnormalAuto, demo
        # A = demo.grcar(32).todense()
        pseudo = NonnormalAuto(A, 1e-5, 1)
        pseudo.plot([10**k for k in range(-4, 0)], spectrum=eigvals(A))

    # base = '/home/gdetorak/NMI-lab/hebb_cd/CHL/data/'
    # fname = base+'emnist/normal_w2_hebb_emnist.dat'
    # fname = base+'bars_stripes/g_init_gaussian_bs_0.010000.dat'
    # fname = base+'bars_stripes/g_init_uniform_bs_1.000000.dat'
    # A = read_from_file(fname).reshape(256, 128)
    w, v = np.linalg.eig(np.dot(np.matrix(A).H, A))
    u, s, v = np.linalg.svd(A)
    At = np.linalg.pinv(A)
    cond = np.linalg.norm(A, ord=2) * np.linalg.norm(At, ord=2)
    print("Condition Number of A: {}".format(cond))
    npts = 50
    # aa = 0.8 * np.linalg.norm(A, 1)
    # xmin, xmax, ymin, ymax = -aa, aa, -aa, aa
    # x = np.arange(xmin, xmax, (xmax-xmin) / (npts-1))
    # y = np.arange(ymin, ymax, (ymax-ymin) / (npts-1))
    # X, Y = np.meshgrid(x, y)
    X, Y = np.meshgrid(np.linspace(-1, 1.5, npts),
                       np.linspace(-1, 1.5, npts))

    spectra, Q, R, T, S = rectpsa(A, X, Y, method='svd')

    # np.save('spectra', spectra)
    # np.save('q_matrix', Q)
    # np.save('r_matrix', R)
    # np.save('t_matrix', T)
    # np.save('s_matrix', S)

    plt.figure()
    circles = np.logspace(np.log10(10e-5), np.log10(1), 10)
    # CS = plt.contour(X, Y, spectra+1e-20, levels=circles)
    CS = plt.contour(X, Y, np.log10(spectra+1e-20))
    plt.clabel(CS, inline=1, fontsize=10)
    plt.show()
