# rCHL - This is an implementation of random Contrastive Hebbian Learning
# algorithm given in: Detorakis, Georgios, Travis Bartley, and Emre Neftci,
# "Contrastive Hebbian Learning with Random Feedback Weights",
# arXiv preprint arXiv:1806.07406 (2018).
#
# Copyright (C) 2018  Georgios Is. Detorakis (gdetor@pm.me)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
import numpy as np
import matplotlib.pylab as plt
import torch as tr
from struct import unpack
from autoenc import autoencoder
from sklearn.manifold import TSNE
# from skimage.measure import block_reduce
import umap


TOL = 0.00001
eps = np.finfo(float).eps


def read_from_file(fname):
    with open(fname, "rb") as f:
        c = f.read()
    size = int(len(c) // 8)
    return np.array(unpack('d'*size, c), 'f')


def df(x):
    return np.exp(-x) / (np.exp(-x) + 1)**2


def gradient_ascent_(x0, theta, b, index=0, gamma=0.01, max_iter=10000):
    cur_x = x0

    for i in range(max_iter):
        prev_x = cur_x
        cur_x += gamma * (theta[:, index]
                          * df(np.dot(theta[:, index], prev_x) + b[index]))
    return cur_x


def plot_latent_var(data, targets, ax, number='A', pos=(-70, 100)):
    im = ax.scatter(data[:, 0], data[:, 1], c=targets, cmap='tab10', s=5)
    plt.colorbar(im)
    # ax.set_xlim([-100, 100])
    # ax.set_ylim([-100, 100])
    # ax.set_xticks([])
    # ax.set_yticks([])
    ax.text(pos[0], pos[1], number,
            va='top',
            ha='left',
            fontsize=20,
            weight='bold')


def plot_max_activity(data, ax, number='A', num_images=36):
    im_size_x, im_size_y = 28, 28
    nn_size_x, nn_size_y = 6, 6

    min_, max_ = data.min(), data.max()
    mm = max(abs(min_), abs(max_))

    R = np.zeros((im_size_x*nn_size_x, im_size_y*nn_size_x))
    for i in range(nn_size_x):
        for j in range(nn_size_y):
            tmp = data[i*nn_size_y+j].reshape(28, 28)
            R[i*im_size_x:(i+1)*im_size_x, j*im_size_y:(j+1)*im_size_y] = tmp
    ax.imshow(R, interpolation='bicubic', cmap=plt.cm.gray, aspect='equal',
              origin='upper', vmin=-mm, vmax=mm)
    ax.set_xticks([])
    ax.set_yticks([])
    ax.text(0, -19.0, number,
            va='top',
            ha='left',
            fontsize=20,
            weight='bold')


def plot_test_images(fname, ax, number='A', num_images=100):
    if isinstance(number, str) is False:
        raise 'Number is not a string!'

    im_size_x, im_size_y = 28, 28
    nn_size_x, nn_size_y = 10, 10
    data = []
    for i in range(num_images):
        data.append(read_from_file(fname+str(i)+'.dat'))

    data = np.array(data)
    R = np.zeros((im_size_x*nn_size_x, im_size_y*nn_size_x))
    for i in range(nn_size_x):
        for j in range(nn_size_y):
            tmp = data[i*nn_size_y+j].reshape(28, 28)
            R[i*im_size_x:(i+1)*im_size_x, j*im_size_y:(j+1)*im_size_y] = tmp
    ax.imshow(R, interpolation='nearest', cmap=plt.cm.gray, aspect='equal',
              origin='upper')
    ax.set_xticks([])
    ax.set_yticks([])
    ax.text(0, -29.0, number,
            va='top',
            ha='left',
            fontsize=20,
            weight='bold')


def plot_pytorch_results(ax, number='A', num_images=100):
    if isinstance(number, str) is False:
        raise 'Number is not a string!'

    nn_size_x, nn_size_y = 10, 10
    im_size_x, im_size_y = 28, 28

    W = np.load("pytorch_ae_results.npy")[:num_images, 0, :]
    W = W.reshape(num_images, im_size_x, im_size_y)

    R = np.zeros((im_size_x*nn_size_x, im_size_y*nn_size_x))
    for i in range(nn_size_x):
        for j in range(nn_size_y):
            tmp = W[i*nn_size_y+j]
            R[i*im_size_x:(i+1)*im_size_x, j*im_size_y:(j+1)*im_size_y] = tmp

    ax.imshow(R, interpolation='nearest', cmap=plt.cm.gray, aspect='equal',
              origin='upper')
    ax.set_xticks([])
    ax.set_yticks([])
    ax.text(0, -28, number,
            va='top',
            ha='left',
            fontsize=20,
            weight='bold')


def plot_ae(ax, number='A'):
    ref_error = np.load("pytorch_ae.npy")

    err_e = []
    for i in range(1):
        # fname_err = './data/ae/ae_chl_error_'+str(i)+'.dat'
        fname_err = './data/ae/ae_chl_error_120.dat'
        err_e.append(read_from_file(fname_err))
    err_e = np.array(err_e)
    mu_e, std_e = np.mean(err_e, axis=0), np.std(err_e, axis=0)

    err = []
    for i in range(1):
        # fname_err = './data/ae/ae_rchl_error_'+str(i)+'.dat'
        fname_err = './data/ae/ae_rchl_error_120.dat'
        err.append(read_from_file(fname_err))
    err = np.array(err)
    mu_re, std_re = np.mean(err, axis=0), np.std(err, axis=0)

    error_mu, error_std = [mu_e, mu_re], [std_e, std_re]

    error = []
    error.append(ref_error)
    error.append(mu_e)
    error.append(mu_re)

    epochs = 20
    case = ['AE', 'CHL', 'rCHL']
    zord = [8, 8, 10]
    import matplotlib.colors as colors
    import matplotlib.cm as cmx
    values = range(len(case))
    jet = plt.get_cmap('BrBG')
    cNorm = colors.Normalize(vmin=0, vmax=values[-1])
    scalarMap = cmx.ScalarMappable(norm=cNorm, cmap=jet)

    lines = []
    st = ['x', 'o', 'p']
    for i in range(len(error)):
        colorVal = scalarMap.to_rgba(values[i])
        if i == 1:
            colorVal = (0.5947712418300657,
                        0.8418300653594772,
                        0.8039215686274511, 1.0)
        line = error[i]
        retLine, = ax.plot(line, color=colorVal, lw=2, zorder=zord[i],
                           marker=st[i], label=case[i])
        lines.append(retLine)
    for i in range(len(error_mu)):
        ax.fill_between(np.arange(epochs), error_mu[i]+error_std[i],
                        error_mu[i]-error_std[i],
                        facecolor=scalarMap.to_rgba(values[i]), alpha=0.2,
                        zorder=0)
    ax.set_ylim([0, 0.04])
    ax.set_xlim([0, epochs])

    ax.set_xlabel('Epochs', fontsize=18, weight='bold')
    ax.set_ylabel('Test MSE', fontsize=18, weight='bold')
    ticks = ax.get_xticks().astype('i')
    ax.set_xticklabels(ticks, fontsize=16, weight='bold')
    ticks = np.round(ax.get_yticks(), 3)
    ax.set_yticklabels(ticks, fontsize=16, weight='bold')
    ax.grid()
    ax.legend(loc=4)
    # ax.text(0, .1, number,
    #         va='top',
    #         ha='left',
    #         fontsize=20,
    #         weight='bold')


if __name__ == '__main__':
    M, K = 784, 28

    W0_rchl = read_from_file("./data/ae/best/ae_rchl_w_2_2.dat").reshape(784,
                                                                         36)
    min_rchl, max_rchl = W0_rchl.min(), W0_rchl.max()
    b0_rchl = read_from_file("./data/ae/best/ae_rchl_b_2_2.dat")

    W0_chl = read_from_file("./data/ae/best/ae_chl_w_2_2.dat").reshape(784,
                                                                       36)
    min_chl, max_chl = W0_chl.min(), W0_chl.max()
    b0_chl = read_from_file("./data/ae/best/ae_chl_b_2_2.dat")

    net = tr.load("sim_autoencoder.pt")
    net_ = list(net.modules())
    W0_ae = net_[5].weight.data.cpu().detach().numpy()
    min_ae, max_ae = W0_ae.min(), W0_ae.max()
    b0_ae = net_[5].bias.data.cpu().detach().numpy()

    data_ae = []
    for i in range(36):
        x0 = np.random.uniform(-.01, .01, (M, ))
        x_ = gradient_ascent_(x0, W0_ae, b0_ae, index=i)
        data_ae.append(x_)
    data_ae = np.array(data_ae)

    data_chl = []
    for i in range(36):
        x0 = np.random.uniform(-.01, .01, (M, ))
        x_ = gradient_ascent_(x0, W0_chl, b0_chl, index=i)
        data_chl.append(x_)
    data_chl = np.array(data_chl)

    data_rchl = []
    for i in range(36):
        x0 = np.random.uniform(-.01, .01, (M, ))
        x_ = gradient_ascent_(x0, W0_rchl, b0_rchl, index=i)
        data_rchl.append(x_)
    data_rchl = np.array(data_rchl)

    # ww = W0 / np.sqrt((W0**2).sum(axis=0) + eps)

    fig = plt.figure(figsize=(13, 13))
    fig.subplots_adjust(wspace=0.5, hspace=0.5, bottom=0.18)

    ax = plt.subplot2grid((6, 6), (0, 0), colspan=2, rowspan=2)
    plot_pytorch_results(ax)

    ax = plt.subplot2grid((6, 6), (0, 2), colspan=2, rowspan=2)
    plot_test_images("./data/ae/ae_chl_test_recon_", ax, number='B')

    ax = plt.subplot2grid((6, 6), (0, 4), colspan=2, rowspan=2)
    plot_test_images("./data/ae/ae_rchl_test_recon_", ax, number='C')

    ax = plt.subplot2grid((6, 6), (2, 0), colspan=2, rowspan=2)
    plot_max_activity(data_ae, ax, number='D')

    ax = plt.subplot2grid((6, 6), (2, 2), colspan=2, rowspan=2)
    plot_max_activity(data_chl, ax, number='E')

    ax = plt.subplot2grid((6, 6), (2, 4), colspan=2, rowspan=2)
    plot_max_activity(data_rchl, ax, number='F')

    T = read_from_file("./data/ae/targets.dat")
    base = "./data/ae/ae_chl_proj_recon_"
    d = []
    for i in range(1000):
        d.append(read_from_file(base+str(i)+".dat"))
    D = np.array(d)
    # emb = TSNE(n_components=2, perplexity=20).fit_transform(D)
    emb = umap.UMAP(n_neighbors=15,
                    min_dist=0.1,
                    metric='correlation').fit_transform(D)

    ax = plt.subplot2grid((6, 6), (4, 2), colspan=2, rowspan=2)
    plot_latent_var(emb, T, ax, number='H', pos=(-90, 120))

    base = "./data/ae/ae_rchl_proj_recon_"
    d = []
    for i in range(1000):
        d.append(read_from_file(base+str(i)+".dat"))
    D = np.array(d)
    # emb = TSNE(n_components=2, perplexity=20).fit_transform(D)
    emb = umap.UMAP(n_neighbors=15,
                    min_dist=0.1,
                    metric='correlation').fit_transform(D)

    ax = plt.subplot2grid((6, 6), (4, 4), colspan=2, rowspan=2)
    plot_latent_var(emb, T, ax, number='I', pos=(-90, 120))

    D = np.load("../autoencoder/latent_rep.npy").reshape(600*100, 36)
    T = np.load("../autoencoder/latent_lbls.npy").reshape(600*100)
    # emb = TSNE(n_components=2, perplexity=20).fit_transform(D[:1000])
    emb = umap.UMAP(n_neighbors=15,
                    min_dist=0.1,
                    metric='correlation').fit_transform(D[:1000])

    ax = plt.subplot2grid((6, 6), (4, 0), colspan=2, rowspan=2)
    plot_latent_var(emb, T[:1000], ax, number='G', pos=(-90, 120))

    # plt.savefig("Figure30.pdf", axis="tight")
    plt.show()
