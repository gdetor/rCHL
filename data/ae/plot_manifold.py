# rCHL - This is an implementation of random Contrastive Hebbian Learning
# algorithm given in: Detorakis, Georgios, Travis Bartley, and Emre Neftci,
# "Contrastive Hebbian Learning with Random Feedback Weights",
# arXiv preprint arXiv:1806.07406 (2018).
#
# Copyright (C) 2018  Georgios Is. Detorakis (gdetor@pm.me)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
import numpy as np
import matplotlib.pylab as plt
from matplotlib import offsetbox
from sklearn.manifold import TSNE
from struct import unpack


def read_from_file(fname):
    with open(fname, "rb") as f:
        c = f.read()
    size = int(len(c) // 8)
    return np.array(unpack('d'*size, c), 'f')


def plot_embedding(X, D, T, title=None):
    x_min, x_max = np.min(X, 0), np.max(X, 0)
    X = (X - x_min) / (x_max - x_min)

    plt.figure(figsize=(13, 8))
    ax = plt.subplot(111)
    for i in range(X.shape[0]):
        plt.text(X[i, 0], X[i, 1], str(T[i]),
                 color=plt.cm.Set1(T[i] / 10.),
                 fontdict={'weight': 'bold', 'size': 9})

    if hasattr(offsetbox, 'AnnotationBbox'):
        # only print thumbnails with matplotlib > 1.0
        shown_images = np.array([[1., 1.]])  # just something big
        for i in range(X.shape[0]):
            dist = np.sum((X[i] - shown_images) ** 2, 1)
            if np.min(dist) < 4e-3:
                # don't show points that are too close
                continue
            shown_images = np.r_[shown_images, [X[i]]]
            imagebox = offsetbox.AnnotationBbox(
                offsetbox.OffsetImage(D[i].reshape(14, 14),
                                      cmap=plt.cm.gray_r),
                X[i])
            ax.add_artist(imagebox)
    plt.xticks([]), plt.yticks([])
    if title is not None:
        plt.title(title, fontsize=16, weight="bold")


def plot_latent_var(data, targets):
    plt.figure(figsize=(6, 6))
    plt.scatter(data[:, 0], data[:, 1], s=5, c=targets, cmap='tab10')
    plt.colorbar()


if __name__ == '__main__':
    if 0:
        from skimage.measure import block_reduce
        base = "./data/ae/ae_rchl_test_recon_"
        d = []
        for i in range(1000):
            d.append(read_from_file(base+str(i)+".dat"))
        D = np.array(d)
        D_red = np.zeros((D.shape[0], 196))
        for i, j in enumerate(D):
            D_red[i] = block_reduce(j.reshape(28, 28),
                                    block_size=(2, 2)).flatten()
        T = read_from_file("./data/ae/targets.dat")

        emb = TSNE(n_components=2, perplexity=50).fit_transform(D)
        plot_embedding(emb, D_red, T, title="rCHL MNIST")

    if 1:
        base = "./data/ae/ae_rchl_proj_recon_"
        d = []
        for i in range(1000):
            d.append(read_from_file(base+str(i)+".dat"))
        D = np.array(d)
        T = read_from_file("./data/ae/targets.dat")
        emb = TSNE(n_components=2, perplexity=20).fit_transform(D)

        plot_latent_var(emb, T)

    if 0:
        D = np.load("../autoencoder/latent_rep.npy").reshape(600*100, 36)
        T = np.load("../autoencoder/latent_lbls.npy").reshape(600*100)

        emb = TSNE(n_components=2, perplexity=50).fit_transform(D[:1000])
        plot_latent_var(emb, T[:1000])

    plt.show()
