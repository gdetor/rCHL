# rCHL - This is an implementation of random Contrastive Hebbian Learning
# algorithm given in: Detorakis, Georgios, Travis Bartley, and Emre Neftci,
# "Contrastive Hebbian Learning with Random Feedback Weights",
# arXiv preprint arXiv:1806.07406 (2018).
#
# Copyright (C) 2018  Georgios Is. Detorakis (gdetor@pm.me)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
import numpy as np
import matplotlib.pylab as plt
from struct import unpack


def read_from_file(fname):
    with open(fname, 'rb') as f:
        c = f.read()
    size = int(len(c) // 8)
    return np.array(unpack('d' * size, c), 'd')


def plot_test_images(fname, ax, number='A', num_images=100):
    if isinstance(number, str) is False:
        raise 'Number is not a string!'

    im_size_x, im_size_y = 28, 28
    nn_size_x, nn_size_y = 10, 10
    data = []
    for i in range(num_images):
        data.append(read_from_file(fname+str(i)+'.dat'))

    data = np.array(data)
    R = np.zeros((im_size_x*nn_size_x, im_size_y*nn_size_x))
    for i in range(nn_size_x):
        for j in range(nn_size_y):
            tmp = data[i*nn_size_y+j].reshape(28, 28)
            R[i*im_size_x:(i+1)*im_size_x, j*im_size_y:(j+1)*im_size_y] = tmp
    ax.imshow(R, interpolation='nearest', cmap=plt.cm.gray, aspect='equal',
              origin='upper')
    ax.set_xticks([])
    ax.set_yticks([])
    ax.text(0, -41.0, number,
            va='top',
            ha='left',
            fontsize=20,
            weight='bold')


def plot_ae(ax, number='A'):
    ref_error = np.load("pytorch_ae.npy")

    err_e = []
    for i in range(1):
        # fname_err = './data/ae/ae_chl_error_'+str(i)+'.dat'
        fname_err = './data/ae/ae_chl_error_120.dat'
        err_e.append(read_from_file(fname_err))
    err_e = np.array(err_e)
    mu_e, std_e = np.mean(err_e, axis=0), np.std(err_e, axis=0)

    err = []
    for i in range(1):
        # fname_err = './data/ae/ae_rchl_error_'+str(i)+'.dat'
        fname_err = './data/ae/ae_rchl_error_120.dat'
        err.append(read_from_file(fname_err))
    err = np.array(err)
    mu_re, std_re = np.mean(err, axis=0), np.std(err, axis=0)

    error_mu, error_std = [mu_e, mu_re], [std_e, std_re]

    error = []
    error.append(ref_error)
    error.append(mu_e)
    error.append(mu_re)

    epochs = 20
    case = ['AE', 'CHL', 'rCHL']
    zord = [8, 8, 10]
    import matplotlib.colors as colors
    import matplotlib.cm as cmx
    values = range(len(case))
    jet = plt.get_cmap('BrBG')
    cNorm = colors.Normalize(vmin=0, vmax=values[-1])
    scalarMap = cmx.ScalarMappable(norm=cNorm, cmap=jet)

    lines = []
    st = ['x', 'o', 'p']
    for i in range(len(error)):
        colorVal = scalarMap.to_rgba(values[i])
        if i == 1:
            colorVal = (0.5947712418300657,
                        0.8418300653594772,
                        0.8039215686274511, 1.0)
        line = error[i]
        retLine, = ax.plot(line, color=colorVal, lw=2, zorder=zord[i],
                           marker=st[i], label=case[i])
        lines.append(retLine)
    for i in range(len(error_mu)):
        ax.fill_between(np.arange(epochs), error_mu[i]+error_std[i],
                        error_mu[i]-error_std[i],
                        facecolor=scalarMap.to_rgba(values[i]), alpha=0.2,
                        zorder=0)
    ax.set_ylim([0, 0.04])
    ax.set_xlim([0, epochs])

    ax.set_xlabel('Epochs', fontsize=18, weight='bold')
    ax.set_ylabel('Test MSE', fontsize=18, weight='bold')
    ticks = ax.get_xticks().astype('i')
    ax.set_xticklabels(ticks, fontsize=16, weight='bold')
    ticks = np.round(ax.get_yticks(), 3)
    ax.set_yticklabels(ticks, fontsize=16, weight='bold')
    ax.grid()
    ax.legend(loc=4)
    # ax.text(0, .1, number,
    #         va='top',
    #         ha='left',
    #         fontsize=20,
    #         weight='bold')


def plot_autoencoder(fname, ax, number='A'):
    if isinstance(number, str) is False:
        raise 'Number is not a string!'

    im_size_x, im_size_y = 28, 28
    nn_size_x, nn_size_y = 6, 6

    W = read_from_file(fname).reshape(im_size_x*im_size_y,
                                      nn_size_x*nn_size_y)
    # W /= np.sqrt((W**2).sum(axis=0))
    R = np.zeros((im_size_x*nn_size_x, im_size_y*nn_size_x))
    for i in range(nn_size_x):
        for j in range(nn_size_y):
            ww = W[:, i*nn_size_x+j].reshape(im_size_x, im_size_y)
            R[i*im_size_x:(i+1)*im_size_x, j*im_size_y:(j+1)*im_size_y] = ww

    # fig = plt.figure()
    # ax = fig.add_subplot(111)
    if 1:
        im = ax.imshow(R, interpolation='bicubic', cmap=plt.cm.gray,
                       aspect='equal', vmin=-5, vmax=5)
    else:
        im = ax.imshow(R, interpolation='bicubic', cmap=plt.cm.gray_r,
                       aspect='equal', vmin=-5, vmax=5)
    ax.set_xticks(np.arange(-0.5, im_size_x*nn_size_x-.5, im_size_x))
    ax.set_yticks(np.arange(-0.5, im_size_y*nn_size_y-.5, im_size_y))
    ax.grid(color='r', linestyle='-', linewidth=1.5)
    ax.set_xticklabels([], color='w')
    ax.set_yticklabels([], color='w')
    ax.spines['left'].set_visible(False)
    ax.spines['bottom'].set_visible(False)
    ax.xaxis.set_tick_params(width=0)
    ax.yaxis.set_tick_params(width=0)
    plt.colorbar(im)
    ax.text(0, -24, number,
            va='top',
            ha='left',
            fontsize=20,
            weight='bold')


if __name__ == '__main__':
    if 1:
        fig = plt.figure(figsize=(7, 5))
        fig.subplots_adjust(bottom=0.15, left=0.2)
        ax = fig.add_subplot(111)
        plot_ae(ax, number='A')
        plt.savefig('Figure26.pdf', axis='tight')
        # plt.savefig('Figure22.pdf', axis='tight')

    if 0:
        fig = plt.figure(figsize=(10, 6))
        fig.subplots_adjust(wspace=0.5, hspace=0.5, bottom=0.18)

        ax = plt.subplot2grid((4, 6), (0, 0), colspan=2, rowspan=2)
        plot_test_images("./data/ae/ae_chl_test_image_", ax, number='A')

        ax = plt.subplot2grid((4, 6), (0, 2), colspan=2, rowspan=2)
        plot_test_images("./data/ae/ae_chl_test_recon_", ax, number='B')

        ax = plt.subplot2grid((4, 6), (0, 4), colspan=2, rowspan=2)
        plot_test_images("./data/ae/ae_rchl_test_recon_", ax, number='C')

        ax = plt.subplot2grid((4, 6), (2, 1), colspan=2, rowspan=2)
        plot_autoencoder("./data/ae/ae_chl_w_0_2.dat", ax, number='D')

        ax = plt.subplot2grid((4, 6), (2, 3), colspan=2, rowspan=2)
        plot_autoencoder("./data/ae/ae_rchl_w_120_2.dat", ax, number='E')

        # plt.savefig('Figure23.pdf', axis='tight')
    plt.show()
