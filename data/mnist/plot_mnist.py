# rCHL - This is an implementation of random Contrastive Hebbian Learning
# algorithm given in: Detorakis, Georgios, Travis Bartley, and Emre Neftci,
# "Contrastive Hebbian Learning with Random Feedback Weights",
# arXiv preprint arXiv:1806.07406 (2018).
#
# Copyright (C) 2018  Georgios Is. Detorakis (gdetor@pm.me)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
import numpy as np
import matplotlib.pylab as plt
from struct import unpack


def read_from_file(fname):
    with open(fname, 'rb') as f:
        c = f.read()
    size = int(len(c) // 8)
    return np.array(unpack('d' * size, c), 'd')


def plot_mnist():
    err, acc = [], []
    for i in range(10):
        fname_err = './data/mnist/mnist_chl_error_'+str(i)+'.dat'
        fname_acc = './data/mnist/mnist_chl_accur_'+str(i)+'.dat'
        err.append(read_from_file(fname_err))
        acc.append(read_from_file(fname_acc))
    err = np.array(err)
    acc = np.array(acc)
    mu_e, std_e = np.mean(err, axis=0), np.std(err, axis=0)
    mu_a, std_a = np.mean(acc, axis=0), np.std(acc, axis=0)

    err, acc = [], []
    for i in range(10):
        fname_err = './data/mnist/mnist_rchl_error_'+str(i)+'.dat'
        fname_acc = './data/mnist/mnist_rchl_accur_'+str(i)+'.dat'
        err.append(read_from_file(fname_err))
        acc.append(read_from_file(fname_acc))
    err = np.array(err)
    acc = np.array(acc)
    mu_re, std_re = np.mean(err, axis=0), np.std(err, axis=0)
    mu_ra, std_ra = np.mean(acc, axis=0), np.std(acc, axis=0)
    error_mu, error_std = [mu_e, mu_re], [std_e, std_re]
    accur_mu, accur_std = [mu_a, mu_ra], [std_a, std_ra]

    error, accur = [], []
    # error.append(read_from_file('./data/mnist/normal_nobias_error_mnist.dat'))
    # error.append(read_from_file('./data/mnist/random_nobias_error_mnist.dat'))
    error.append(mu_e)
    error.append(mu_re)
    error.append(np.load('./data/mnist/mlp_mnist_mse.npy'))
    error.append(np.load('./data/mnist/mlp_mnist_fa_mse.npy'))

    # accur.append(read_from_file('./data/mnist/normal_nobias_accur_mnist.dat'))
    # accur.append(read_from_file('./data/mnist/random_nobias_accur_mnist.dat'))
    accur.append(mu_a)
    accur.append(mu_ra)
    accur.append(np.load('./data/mnist/mlp_mnist_acc.npy'))
    accur.append(np.load('./data/mnist/mlp_mnist_fa_acc.npy'))

    case = ['CHL', 'rCHL', 'BP', 'FDA']
    zord = [8, 10, 5, 0]
    import matplotlib.colors as colors
    import matplotlib.cm as cmx
    values = range(len(case))
    jet = plt.get_cmap('BrBG')
    cNorm = colors.Normalize(vmin=0, vmax=values[-1])
    scalarMap = cmx.ScalarMappable(norm=cNorm, cmap=jet)

    fig = plt.figure(figsize=(12, 4))
    fig.subplots_adjust(wspace=0.3, hspace=0.3, bottom=0.18)
    ax = fig.add_subplot(121)

    lines = []
    st = ['x', 'o', 'p', '^']
    for i in range(len(error)):
        colorVal = scalarMap.to_rgba(values[i])
        if i <= 1:
            line = error[i]
            retLine, = ax.plot(line, color=colorVal, lw=2, zorder=zord[i],
                               marker=st[i])
        if i > 1:
            line = error[i].min() * np.ones((50,))
            retLIne, = ax.plot(line, color=colorVal, lw=2, zorder=zord[i],
                               ls='--')
        lines.append(retLine)
    for i in range(len(error_mu)):
        ax.fill_between(np.arange(40), error_mu[i]+error_std[i],
                        error_mu[i]-error_std[i],
                        facecolor=scalarMap.to_rgba(values[i]), alpha=0.6,
                        zorder=0)
    ax.set_ylim([0, 0.01])
    ax.set_xlim([0, 40])

    ax.set_xlabel('Epochs', fontsize=18, weight='bold')
    ax.set_ylabel('Averaged Test MSE', fontsize=18, weight='bold')
    ticks = ax.get_xticks().astype('i')
    ax.set_xticklabels(ticks, fontsize=16, weight='bold')
    ticks = np.round(ax.get_yticks(), 3)
    ax.set_yticklabels(ticks, fontsize=16, weight='bold')
    ax.grid()
    ax.text(0, 0.033, 'A',
            va='top',
            ha='left',
            fontsize=22,
            weight='bold')

    ax = fig.add_subplot(122)
    lines = []
    for i in range(len(accur)):
        colorVal = scalarMap.to_rgba(values[i])
        if i <= 1:
            line = accur[i]
            retLine, = ax.plot(line, color=colorVal, lw=2,
                               label=str(case[i]), zorder=zord[i])
        if i > 1:
            line = accur[i].max() * np.ones((50,))
            retLIne, = ax.plot(line, color=colorVal, lw=2, zorder=zord[i],
                               ls='--', label=str(case[i]))
        lines.append(retLine)
    for i in range(len(error_mu)):
        ax.fill_between(np.arange(40), accur_mu[i]+accur_std[i],
                        accur_mu[i]-accur_std[i],
                        facecolor=scalarMap.to_rgba(values[i]), alpha=0.6,
                        zorder=0)
    ax.set_ylim([0.9, 1])
    ax.set_xlim([0, 40])

    ax.set_xlabel('Epochs', fontsize=18, weight='bold')
    ax.set_ylabel('Averaged Test Accuracy', fontsize=18, weight='bold')
    ticks = ax.get_xticks().astype('i')
    ax.set_xticklabels(ticks, fontsize=16, weight='bold')
    ticks = np.round(ax.get_yticks(), 2)
    ax.set_yticklabels(ticks, fontsize=16, weight='bold')
    ax.grid()
    ax.legend(loc=3, bbox_to_anchor=(1.0, 0.8), ncol=1)
    ax.text(0, 1.09, 'B',
            va='top',
            ha='left',
            fontsize=22,
            weight='bold')

    # plt.savefig('Figure25.pdf', axis='tight')


if __name__ == '__main__':
    plot_mnist()
    plt.show()
