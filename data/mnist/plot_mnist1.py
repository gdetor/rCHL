# rCHL - This is an implementation of random Contrastive Hebbian Learning
# algorithm given in: Detorakis, Georgios, Travis Bartley, and Emre Neftci,
# "Contrastive Hebbian Learning with Random Feedback Weights",
# arXiv preprint arXiv:1806.07406 (2018).
#
# Copyright (C) 2018  Georgios Is. Detorakis (gdetor@pm.me)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
import numpy as np
import matplotlib.pylab as plt
from struct import unpack


def read_from_file(fname):
    with open(fname, 'rb') as f:
        c = f.read()
    size = int(len(c) // 8)
    return np.array(unpack('d' * size, c), 'd')


def plot_mnist():
    error, accur = [], []
    error.append(read_from_file('./data/mnist/normal_nobias_error_mnist.dat'))
    error.append(read_from_file('./data/mnist/random_nobias_error_mnist.dat'))
    error.append(np.load('./data/mnist/mlp_mnist_mse.npy'))
    error.append(np.load('./data/mnist/mlp_mnist_fa_mse.npy'))

    accur.append(read_from_file('./data/mnist/normal_nobias_accur_mnist.dat'))
    accur.append(read_from_file('./data/mnist/random_nobias_accur_mnist.dat'))
    accur.append(np.load('./data/mnist/mlp_mnist_acc.npy'))
    accur.append(np.load('./data/mnist/mlp_mnist_fa_acc.npy'))

    case = ['CHL', 'rCHL', 'BP', 'FDA']
    zord = [8, 10, 5, 0]
    import matplotlib.colors as colors
    import matplotlib.cm as cmx
    values = range(len(case))
    jet = plt.get_cmap('BrBG')
    cNorm = colors.Normalize(vmin=0, vmax=values[-1])
    scalarMap = cmx.ScalarMappable(norm=cNorm, cmap=jet)

    fig = plt.figure(figsize=(12, 8))
    fig.subplots_adjust(wspace=0.4, hspace=0.4, bottom=0.18)
    ax = fig.add_subplot(221)

    lines = []
    st = ['x', 'o', 'p', '^']
    for i in range(len(error)):
        colorVal = scalarMap.to_rgba(values[i])
        if i <= 1:
            line = error[i]
            retLine, = ax.plot(line, color=colorVal, lw=2, zorder=zord[i],
                               marker=st[i])
        if i > 1:
            line = error[i].min() * np.ones((50,))
            retLIne, = ax.plot(line, color=colorVal, lw=2, zorder=zord[i],
                               ls='--')
        lines.append(retLine)
    ax.set_ylim([0, 0.01])
    ax.set_xlim([0, 20])

    ax.set_xlabel('Epochs', fontsize=18, weight='bold')
    ax.set_ylabel('Test MSE', fontsize=18, weight='bold')
    ticks = ax.get_xticks().astype('i')
    ax.set_xticklabels(ticks, fontsize=16, weight='bold')
    ticks = ax.get_yticks()
    ax.set_yticklabels(ticks, fontsize=16, weight='bold')
    ax.grid()
    ax.text(0, 0.0115, 'A',
            va='top',
            ha='left',
            fontsize=22,
            weight='bold')
    ax.text(15, 0.002, 'MNIST',
            va='top',
            ha='left',
            fontsize=14,
            weight='bold')

    ax = fig.add_subplot(222)
    lines = []
    for i in range(len(accur)):
        colorVal = scalarMap.to_rgba(values[i])
        if i <= 1:
            line = accur[i]
            retLine, = ax.plot(line, color=colorVal, lw=2,
                               label=str(case[i]), zorder=zord[i])
        if i > 1:
            line = accur[i].max() * np.ones((50,))
            retLIne, = ax.plot(line, color=colorVal, lw=2, zorder=zord[i],
                               ls='--', label=str(case[i]))
        lines.append(retLine)
    ax.set_ylim([0.9, 1])
    ax.set_xlim([0, 20])

    ax.set_xlabel('Epochs', fontsize=18, weight='bold')
    ax.set_ylabel('Test Accuracy', fontsize=18, weight='bold')
    ticks = ax.get_xticks().astype('i')
    ax.set_xticklabels(ticks, fontsize=16, weight='bold')
    ticks = np.round(ax.get_yticks(), 2)
    ax.set_yticklabels(ticks, fontsize=16, weight='bold')
    ax.grid()
    ax.legend(loc=3, bbox_to_anchor=(1.0, 0.6), ncol=1)
    ax.text(0, 1.015, 'B',
            va='top',
            ha='left',
            fontsize=22,
            weight='bold')
    ax.text(16, 0.92, 'MNIST',
            va='top',
            ha='left',
            fontsize=14,
            weight='bold')

    error, accur = [], []
    error.append(read_from_file('./data/emnist/normal_error_emnist.dat'))
    error.append(read_from_file('./data/emnist/random_error_emnist.dat'))
    error.append(np.load('./data/emnist/mlp_emnist_mse.npy'))
    error.append(np.load('./data/emnist/mlp_emnist_fa_mse.npy'))

    accur.append(read_from_file('./data/emnist/normal_accur_emnist.dat'))
    accur.append(read_from_file('./data/emnist/random_accur_emnist.dat'))
    accur.append(np.load('./data/emnist/mlp_emnist_acc.npy'))
    accur.append(np.load('./data/emnist/mlp_emnist_fa_acc.npy'))

    case = ['CHL', 'rCHL', 'BP', 'FDA']
    import matplotlib.colors as colors
    import matplotlib.cm as cmx
    values = range(len(case))
    jet = plt.get_cmap('BrBG')
    cNorm = colors.Normalize(vmin=0, vmax=values[-1])
    scalarMap = cmx.ScalarMappable(norm=cNorm, cmap=jet)

    ax = fig.add_subplot(223)
    np.set_printoptions(suppress=True)

    lines = []
    zord = [8, 10, 5, 0]
    st = ['x', 'o', 'p', '^']
    for i in range(len(error)):
        colorVal = scalarMap.to_rgba(values[i])
        if i <= 1:
            line = error[i]
            retLine, = ax.plot(line, color=colorVal, lw=2, zorder=zord[i],
                               marker=st[i])
        if i > 1:
            line = error[i].min() * np.ones((50,))
            retLIne, = ax.plot(line, color=colorVal, lw=2, zorder=zord[i],
                               ls='--')
        lines.append(retLine)
    ax.set_ylim([0, 0.02])
    ax.set_xlim([0, 40])

    ax.set_xlabel('Epochs', fontsize=18, weight='bold')
    ax.set_ylabel('Test MSE', fontsize=18, weight='bold')
    ticks = ax.get_xticks().astype('i')
    ax.set_xticklabels(ticks, fontsize=16, weight='bold')
    ticks = np.round(ax.get_yticks(), 3)
    ax.set_yticklabels(ticks, fontsize=16, weight='bold')
    ax.grid()
    ax.text(0, 0.023, 'C',
            va='top',
            ha='left',
            fontsize=22,
            weight='bold')
    ax.text(30, 0.003, 'eMNIST',
            va='top',
            ha='left',
            fontsize=14,
            weight='bold')

    ax = fig.add_subplot(224)
    lines = []
    for i in range(len(accur)):
        colorVal = scalarMap.to_rgba(values[i])
        print(colorVal)
        if i <= 1:
            line = accur[i]
            retLine, = ax.plot(line, color=colorVal, lw=2,
                               label=str(case[i]), zorder=zord[i])
        if i > 1:
            line = accur[i].max() * np.ones((50,))
            retLIne, = ax.plot(line, color=colorVal, lw=2, zorder=zord[i],
                               ls='--', label=str(case[i]))
        lines.append(retLine)
    ax.set_ylim([0.7, 1])
    ax.set_xlim([0, 40])

    ax.set_xlabel('Epochs', fontsize=18, weight='bold')
    ax.set_ylabel('Test Accuracy', fontsize=18, weight='bold')
    ticks = ax.get_xticks().astype('i')
    ax.set_xticklabels(ticks, fontsize=16, weight='bold')
    ticks = np.round(ax.get_yticks(), 2)
    ax.set_yticklabels(ticks, fontsize=16, weight='bold')
    ax.grid()
    ax.text(0, 1.05, 'D',
            va='top',
            ha='left',
            fontsize=22,
            weight='bold')
    ax.text(30, 0.75, 'eMNIST',
            va='top',
            ha='left',
            fontsize=14,
            weight='bold')
    # plt.savefig('Figure10.pdf', axis='tight')


if __name__ == '__main__':
    plot_mnist()
    plt.show()
