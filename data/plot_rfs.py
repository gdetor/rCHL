# rCHL - This is an implementation of random Contrastive Hebbian Learning
# algorithm given in: Detorakis, Georgios, Travis Bartley, and Emre Neftci,
# "Contrastive Hebbian Learning with Random Feedback Weights",
# arXiv preprint arXiv:1806.07406 (2018).
#
# Copyright (C) 2018  Georgios Is. Detorakis (gdetor@pm.me)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
import sys
import numpy as np
import matplotlib.pylab as plt
from struct import unpack


def read_bin_file(fname, n=18, m=100):
    with open(fname, 'rb') as f:
        c = f.read()
    size = len(c)
    tmp = np.array(unpack('d'*int(size // 8), c), 'd')
    return tmp


if __name__ == '__main__':
    im_size_x, im_size_y = int(sys.argv[2]), int(sys.argv[2])
    nn_size_x, nn_size_y = int(sys.argv[3]), int(sys.argv[4])

    W = read_bin_file(sys.argv[1]).reshape(im_size_x*im_size_y,
                                           nn_size_x*nn_size_y)
    R = np.zeros((im_size_x*nn_size_x, im_size_y*nn_size_x))
    for i in range(nn_size_x):
        for j in range(nn_size_y):
            ww = W[:, i*nn_size_x+j].reshape(im_size_x, im_size_y)
            R[i*im_size_x:(i+1)*im_size_x, j*im_size_y:(j+1)*im_size_y] = ww

    fig = plt.figure()
    ax = fig.add_subplot(111)
    if sys.argv[5] != 'b':
        im = ax.imshow(R, interpolation='bicubic', cmap=plt.cm.gray)
    else:
        im = ax.imshow(R, interpolation='bicubic', cmap=plt.cm.gray_r)
    ax.set_xticks(np.arange(-0.5, im_size_x*nn_size_x-.5, im_size_x))
    ax.set_yticks(np.arange(-0.5, im_size_y*nn_size_y-.5, im_size_y))
    ax.grid(color='r', linestyle='-', linewidth=1.5)
    ax.set_xticklabels([])
    ax.set_yticklabels([])
    plt.colorbar(im)
    plt.show()
