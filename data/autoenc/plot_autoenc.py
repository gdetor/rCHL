# rCHL - This is an implementation of random Contrastive Hebbian Learning
# algorithm given in: Detorakis, Georgios, Travis Bartley, and Emre Neftci,
# "Contrastive Hebbian Learning with Random Feedback Weights",
# arXiv preprint arXiv:1806.07406 (2018).
#
# Copyright (C) 2018  Georgios Is. Detorakis (gdetor@pm.me)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
import numpy as np
from struct import unpack
import matplotlib.pylab as plt


def read_from_file(fname):
    with open(fname, 'rb') as f:
        c = f.read()
    size = int(len(c) // 8)
    return np.array(unpack('d' * size, c), 'd')


def plot_autoencoder(fname, ax, number='A'):
    if isinstance(number, str) is False:
        raise 'Number is not a string!'

    im_size_x, im_size_y = 28, 28
    nn_size_x, nn_size_y = 6, 6

    W = read_from_file(fname).reshape(im_size_x*im_size_y,
                                      nn_size_x*nn_size_y)
    R = np.zeros((im_size_x*nn_size_x, im_size_y*nn_size_x))
    for i in range(nn_size_x):
        for j in range(nn_size_y):
            ww = W[:, i*nn_size_x+j].reshape(im_size_x, im_size_y)
            R[i*im_size_x:(i+1)*im_size_x, j*im_size_y:(j+1)*im_size_y] = ww

    # fig = plt.figure()
    # ax = fig.add_subplot(111)
    if 1:
        im = ax.imshow(R, interpolation='bicubic', cmap=plt.cm.gray)
    else:
        im = ax.imshow(R, interpolation='bicubic', cmap=plt.cm.gray_r)
    ax.set_xticks(np.arange(-0.5, im_size_x*nn_size_x-.5, im_size_x))
    ax.set_yticks(np.arange(-0.5, im_size_y*nn_size_y-.5, im_size_y))
    ax.grid(color='r', linestyle='-', linewidth=1.5)
    ax.set_xticklabels([], color='w')
    ax.set_yticklabels([], color='w')
    ax.spines['left'].set_visible(False)
    ax.spines['bottom'].set_visible(False)
    ax.xaxis.set_tick_params(width=0)
    ax.yaxis.set_tick_params(width=0)
    plt.colorbar(im)
    ax.text(0, -24, number,
            va='top',
            ha='left',
            fontsize=20,
            weight='bold')


def plot_test_images(fname, ax, number='A'):
    if isinstance(number, str) is False:
        raise 'Number is not a string!'

    data = []
    for i in range(9):
        data.append(read_from_file(fname+str(i)+'.dat'))

    data = np.array(data)
    R = np.zeros((28, 9*28))
    for i in range(9):
        R[:, i*28:(i+1)*28] = data[i, :].reshape(28, 28)

    ax.imshow(R, interpolation='nearest', cmap=plt.cm.gray,
              origin='upper')
    ax.set_xticks([])
    ax.set_yticks([])
    ax.text(0, -14.0, number,
            va='top',
            ha='left',
            fontsize=20,
            weight='bold')


if __name__ == '__main__':
    random_1 = './data/autoenc/random_hebb_w_1.dat'
    random_2 = './data/autoenc/random_hebb_w_2.dat'

    fig = plt.figure(figsize=(6, 6))

    nfname = './data/autoenc/normal_test_image_'
    ax = plt.subplot2grid((5, 4), (0, 0), colspan=4)
    plot_test_images(nfname, ax, 'A')

    rfname = './data/autoenc/normal_test_recon_'
    ax = plt.subplot2grid((5, 4), (1, 0), colspan=4)
    plot_test_images(rfname, ax, 'B')

    rfname = './data/autoenc/random_test_recon_'
    ax = plt.subplot2grid((5, 4), (2, 0), colspan=4)
    plot_test_images(rfname, ax, 'C')

    normal_1 = './data/autoenc/normal_hebb_w_1.dat'
    normal_2 = './data/autoenc/normal_hebb_w_2.dat'
    ax = plt.subplot2grid((5, 4), (3, 0), colspan=2, rowspan=2)
    plot_autoencoder(normal_2, ax, 'D')

    random_1 = './data/autoenc/random_hebb_w_1.dat'
    random_2 = './data/autoenc/random_hebb_w_2.dat'
    ax = plt.subplot2grid((5, 4), (3, 2), colspan=2, rowspan=2)
    plot_autoencoder(random_2, ax, 'E')

    plt.savefig('Figure15.pdf', axis='tight')
    plt.show()
