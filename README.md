random Contrastive Hebbian Learning
===================================

This repository provides the implementation of the random Contrastive 
Hebbian Learning (rCHL) algorithm [1]. 

Files
-----

In the src/ directory there are all the .c files. The rCHL implementation is in
the hebb_cd.c file. The pcg_basic.c implements the PCG random number generator
(http://www.pcg-random.org/). In the file load_data.c there are all the necessary
functions to load the (e)MNIST and the Bars and Stripes data sets. The file 
analysis.c used for running different experiments for the paper [1]. 
All the header files are in the include/ directory. 
In the folder data/ there are all the Python scripts to plot the figures from 
the paper [1]. 

Dependencies
------------
* PCG random number generator (it comes with the source code)
* CBLAS (http://www.netlib.org/blas/)
* GNU Parallel (https://www.gnu.org/software/parallel/)


Examples
--------
1. ae.c - autoencoder
2. barstripes.c - bars and stripes classification (requires the Bars & Stripes data set)
3. mnist.c - MNIST digits classification (requires the MNIST data set)
4. emnist.c - eMNIST letters classification (requires the EMNIST data set)
5. xor.c - XOR perceptron
6. test_ae - Tests the learned AE representations


Run the code
------------
The user can choose between the CHL and rCHL algorithms by setting the macro
RANDOM to 0 or 1, respectively, in the common.h file. In order to compile an
example the user has to type:
```bash
$ make xor      (for the xor example)
```
and then it runs by
```bash
$ ./bin/xor 12334 0
```
The first argument is the seed for the random number generator and the second one
is the number of the experiment. These two numbers can be used when the user 
would like to run multiple experiments (use the run.sh file). 

Run multiple iterations of the same experiment
----------------------------------------------
If one would like to run multiple experiments with let's say different seeds
they can use the run.sh script. This scripts requires the GNU Parallel to run. 
The following command will run 10 different experiments for the XOR network, 
```bash
$ ./run.sh ./bin/xor 10
```


Platform Information
--------------------
The code was developed and ran on a platform with the following specs:

```
Platform: linux
Python: 3.7.0 (default, Sep 15 2018, 19:13:07) 
[GCC 8.2.1 20180831]
Machine and architecture x86_64 64bit 
NumPy: 1.15.2
SciPy: 1.1.0
matplotlib: 2.2.3
```

[1] Detorakis, Georgios, Travis Bartley, and Emre Neftci, 
*Contrastive Hebbian Learning with Random Feedback Weights*, 
arXiv preprint arXiv:1806.07406 (2018).
