IDIR := ./include
ODIR := ./obj
SRC := ./src
BIN := ./bin
DATADIR := ./data

CC=gcc
CFLAGS= -Ofast -flto -fstrict-aliasing -march=native -mtune=native -I$(IDIR) 
LIBS=-lm -llapack -lblas -lcblas

xor: $(SRC)/xor.c $(SRC)/rchl.c $(SRC)/functions.c $(SRC)/pcg_basic.c
	gcc -o $(BIN)/$@ $^ $(CFLAGS) $(LIBS)

analysis: $(SRC)/analysis.c $(SRC)/rchl.c $(SRC)/functions.c $(SRC)/pcg_basic.c $(SRC)/load_data.c
	gcc -o $(BIN)/$@ $^ $(CFLAGS) $(LIBS)

mnist: $(SRC)/mnist.c $(SRC)/rchl.c $(SRC)/functions.c $(SRC)/pcg_basic.c $(SRC)/load_data.c
	gcc -o $(BIN)/$@ $^ $(CFLAGS) $(LIBS)

emnist: $(SRC)/emnist.c $(SRC)/rchl.c $(SRC)/functions.c $(SRC)/pcg_basic.c $(SRC)/load_data.c
	gcc -o $(BIN)/$@ $^ $(CFLAGS) $(LIBS)

ae: $(SRC)/ae.c $(SRC)/rchl.c $(SRC)/functions.c $(SRC)/pcg_basic.c $(SRC)/load_data.c
	gcc -o $(BIN)/$@ $^ $(CFLAGS) $(LIBS)

test_ae: $(SRC)/test_ae.c $(SRC)/rchl.c $(SRC)/functions.c $(SRC)/pcg_basic.c $(SRC)/load_data.c
	gcc -o $(BIN)/$@ $^ $(CFLAGS) $(LIBS)

project_ae: $(SRC)/project_ae.c $(SRC)/rchl.c $(SRC)/functions.c $(SRC)/pcg_basic.c $(SRC)/load_data.c
	gcc -o $(BIN)/$@ $^ $(CFLAGS) $(LIBS)

barstripes: $(SRC)/barstripes.c $(SRC)/rchl.c $(SRC)/functions.c $(SRC)/pcg_basic.c $(SRC)/load_data.c
	gcc -o $(BIN)/$@ $^ $(CFLAGS) $(LIBS)

.PHONY: clean

clean:
	rm -f $(ODIR)/*.o *~ core $(INCDIR)/*~ \
	rm -f $(SRC)/*.o \
	rm -f $(BIN)/*
