/* rCHL - This is an implementation of random Contrastive Hebbian Learning 
 * algorithm given in: Detorakis, Georgios, Travis Bartley, and Emre Neftci, 
 * "Contrastive Hebbian Learning with Random Feedback Weights",
 *  arXiv preprint arXiv:1806.07406 (2018).
 *  
 *  Copyright (C) 2018  Georgios Is. Detorakis (gdetor@pm.me)
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *  */
#ifndef HEBB_CD_H
#define HEBB_CD_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <float.h>
#include <math.h>
#include <sys/mman.h>
#include <time.h>

#include "pcg_basic.h"
#include "math_fun.h"

#include <cblas.h>

#define RANDOM 1
#define NORMAL 0
#define BIAS 0

#define alloc(type_t, size) (type_t *) malloc((size) * sizeof(type_t))
//#define alloc_(type_t, size, align) (type_t *) aligned_alloc(align, (size)*sizeof(type_t))
#define alloc_zeros(type_t, size) (type_t *) calloc(size, sizeof(type_t))
#define mem_test(mem) {if(!mem) { \
                         perror("Cannot allocate memory!\n");   \
                         exit(-2);} }
#define dealloc(ptr) {free(ptr); \
                      ptr = NULL; }

#define XORSWAP(a, b)   ((&(a) == &(b)) ? (a) : ((a)^=(b),(b)^=(a),(a)^=(b)))

/* DGESVD prototype */
extern void dgesvd_(char* jobu, char* jobvt, int* m, int* n, double* a,
                int* lda, double* s, double* u, int* ldu, double* vt, int* ldvt,
                double* work, int* lwork, int* info);


struct layer_s {
    double *x;
    double *x_pos;
    double *x_neg;
    double *W;
    double *G;
    double *b;
    double *tmp;
    double *tmp_t;
    double *dW_hat;
    double *dW_chk;
    double *db_hat;
    double *db_chk;
    size_t size;
    double (*f)(double);
    char pad[16];
} __attribute__ ((aligned));

typedef struct layer_s layer;


/* Files I/O functions */
double **read_mnist_images(size_t, char *);
double **read_mnist_labels(size_t, size_t, char *);
double **read_sin_data(char *, int *, int *);
void read_bst_images(char *, double ***, double ***, size_t, size_t);
void write2file(char *, double *, size_t);
void read_from_file(char *, double **, size_t);
void mask_data(double **, size_t, size_t, int);
void transpose(char *, double **restrict, size_t, size_t);


/* Printing functions */
void print_mat(double *, size_t, size_t);
char *concatenate(char *, double);
char *get_name(char *, char *, long long int);


/* RNG related functions */
double uniform(double, double, pcg32_random_t *);
void uniform_array(double **, double, double, size_t, pcg32_random_t *);
double normal(double, double);
void normal_array(double **, double, double, size_t);


/* General purpose functions */
void allocate(layer *, double, double, pcg32_random_t *, const size_t);
void deallocate(layer *, const size_t);


/* rCHL and CHL functions */
void forward_pass(layer *, const double, const double, const size_t,
                  const size_t);
void backward_pass(layer *,const double, const double, const size_t,
                   const size_t);
void update_weights(layer *, double *, const size_t);
void svd_(double *, const size_t, const size_t, double *, double *);
void spectral_radius(double **, const size_t, const size_t);

/* Experiments functions */
void run_xor(double, double, long long int, int, int, int, char *);
void run_analysis(double, double, long long int, int, int, int, char *);
void run_bars_stipes(double, double, double, double, long long int, int,
                     int, char *);
void run_mnist(double, double, long long int, int, int, int, int, char *);
void run_emnist(double, double, long long int, int, int, int, int, char *);
void run_prediction(int, double, double);
void run_ae(double, double, long long int, int, int, int, int, char *);
void test_autoencoder(double, double);
void test_prediction(double, double);

#endif  /*HEBB_CD_H*/ 
