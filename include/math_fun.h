/* rCHL - This is an implementation of random Contrastive Hebbian Learning 
 * algorithm given in: Detorakis, Georgios, Travis Bartley, and Emre Neftci, 
 * "Contrastive Hebbian Learning with Random Feedback Weights",
 *  arXiv preprint arXiv:1806.07406 (2018).
 *  
 *  Copyright (C) 2018  Georgios Is. Detorakis (gdetor@pm.me)
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *  */
#ifndef MATH_FUN_H
#define MATH_FUN_H

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

inline int reverse_int (int x) {
    unsigned char ch1, ch2, ch3, ch4;

    ch1 = x & 255;
    ch2 = (x >> 8) & 255;
    ch3 = (x >> 16) & 255;
    ch4 = (x >> 24) & 255;

    return((int)ch1<<24)+((int)ch2<<16)+((int)ch3<<8)+ch4;
}


inline double mse(double *restrict y,
                  double *restrict y_hat,
                  size_t len)
{
    size_t i;
    double mysum = 0, tmp;

    for(i = 0; i < len; ++i) {
        tmp = y[i] - y_hat[i];
        mysum += tmp * tmp;
    }

    mysum /= (double) len;

    return mysum;
}


inline double f(double x)
{
    return 1.0 / (1.0 + exp(-x)); 
}


inline double g(double x)
{
    return tanh(x);
}


inline double h(double x)
{
    return (x > 0) ? x : 0;
}


inline double z(double x)
{
    return x;
}


inline int argmax(double *x, size_t len) 
{
    size_t i;
    int index = 0;
    double max = 0;

    max = x[0];
    for(i = 0; i < len; ++i) {
        if (x[i] > max) {
            index = (int) i;
            max = x[i];
        }
    }
    return index;
}


inline int argmin(double *x, size_t len)
{
    size_t i;
    int index;
    double min;

    min = x[0];
    for (i = 0; i < len; ++i) {
        if (x[i] < min) {
            index = (int) i;
            min = x[i];
        }
    }
    return index;
}


inline double max(double *restrict y, size_t len)
{
    size_t i;
    double m = y[0];

    for (i = 1; i < len; ++i) {
        if (y[i] > m)
            m = y[i];
    }

    return m;
}


inline double min(double *restrict y, size_t len)
{
    size_t i;
    double m = y[0];

    for (i = 1; i < len; ++i) {
        if (y[i] < m)
            m = y[i];
    }

    return m;
}

#endif  /* MATH_FUN_H */
